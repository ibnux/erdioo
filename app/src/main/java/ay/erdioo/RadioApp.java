package ay.erdioo;

import android.os.StrictMode;

import androidx.multidex.MultiDexApplication;

import com.androidnetworking.AndroidNetworking;

import ay.erdioo.data.RadioData;
import cat.ereza.customactivityoncrash.config.CaocConfig;
/**
 * Created by ibnumaksum on 4/16/15.
 */
public class RadioApp extends MultiDexApplication {

    public static RadioData nowRadio;
    public static String nowPlaying="";
    public static RadioApp app;
    public static boolean isClicked = false;
    public static int clicked = 0;
    public static boolean disableIklan = false;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //default: CaocConfig.BACKGROUND_MODE_SHOW_CUSTOM
                .enabled(true) //default: true
                .showErrorDetails(true) //default: true
                .showRestartButton(true) //default: true
                .logErrorOnRestart(true) //default: true
                .trackActivities(true) //default: false
                .minTimeBetweenCrashesMs(2000) //default: 3000
                .apply();
        AndroidNetworking.initialize(getApplicationContext());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

}
