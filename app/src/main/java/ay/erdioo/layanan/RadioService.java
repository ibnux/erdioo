package ay.erdioo.layanan;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataOutput;
import com.google.android.exoplayer2.metadata.icy.IcyInfo;

import ay.erdioo.R;
import ay.erdioo.activity.RadioActivity;
import ay.erdioo.data.RadioData;
import ay.erdioo.utils.Utils;

public class RadioService extends Service implements Player.EventListener, MetadataOutput {
    public static final String ACTION_PLAY = "ay.erdioo.action.PLAY";
    public static final String ACTION_STOP = "ay.erdioo.action.STOP";
    SimpleExoPlayer aacMp3Player=null;
    WifiManager.WifiLock wifiLock=null;
    String judulLagu="",idRadio="",aksinya="";
    RadioData radioData = new RadioData();
    public static boolean isPlaying = false;
    boolean isOnGetMeta = false, hasMetadata=false;
    public RadioService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
//        try {
//            java.net.URL.setURLStreamHandlerFactory(new java.net.URLStreamHandlerFactory() {
//                public java.net.URLStreamHandler createURLStreamHandler(String protocol) {
//                    Utils.log("Asking for stream handler for protocol: '" + protocol + "'");
//                    if ("icy".equals(protocol))
//                        return new com.spoledge.aacdecoder.IcyURLStreamHandler();
//                    return null;
//                }
//            });
//        } catch (Throwable t) {
//            Utils.log("Cannot set the ICY URLStreamHandler - maybe already set ? - " + t);
//        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!=null && intent.getAction().equals(ACTION_PLAY)) {
            if(aacMp3Player==null)
                createPlayer();
            if(!idRadio.equals(intent.getStringExtra("idRadio")) || !isPlaying) {
                aksinya="";
                radioData.setJson(intent.getStringExtra("dataRadio"));
                idRadio = intent.getStringExtra("idRadio");
                aacMp3Player.stop();
                stopForeground(true);
                if(createPlayer()) {
                    try {
                        Utils.log("Playing: " + radioData.getUrlStreaming());
                        aacMp3Player.addMediaItem(MediaItem.fromUri("https://api.erdioo.net/iklan.mp3?" + ((int) System.currentTimeMillis() / 1000000)));
                        aacMp3Player.addMediaItem(MediaItem.fromUri(radioData.getUrlStreaming()));
                        //aacMp3Player.playAsync("https://sportku.com:8000/genfm");
                        //aacMp3Player.playAsync("https://rama-fm.simaya.net.id:8800/;");
                        aacMp3Player.addMetadataOutput(this);
                        aacMp3Player.addListener(this);
                        aacMp3Player.prepare();
                        aacMp3Player.play();
                        connectingNotif();
                        kirimSinyal("connecting", "");
                    }catch (Exception e){
                        Utils.showToast("Gagal Menjalankan Radio",this);
                        kirimSinyal("error", "Gagal Menjalankan Radio");
                        lockWifi(false);
                        stopForeground(true);
                        stopSelf();
                    }
                }else{
                    Utils.showToast("Gagal Menjalankan Radio",this);
                    kirimSinyal("error", "Gagal Menjalankan Radio");
                    lockWifi(false);
                    stopForeground(true);
                    stopSelf();
                }
            }
        }else if (intent!=null && intent.getAction().equals(ACTION_STOP)) {
            if(aacMp3Player!=null)
                aacMp3Player.stop();
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public boolean createPlayer(){
        try {
//            RenderersFactory audioOnlyRenderersFactory = new RenderersFactory() {
//                @Override
//                public Renderer[] createRenderers(Handler eventHandler, VideoRendererEventListener videoRendererEventListener, AudioRendererEventListener audioRendererEventListener, TextOutput textRendererOutput, MetadataOutput metadataRendererOutput) {
//                    return new Renderer[]{new MediaCodecAudioRenderer(
//                            RadioService.this, MediaCodecSelector.DEFAULT, eventHandler, audioRendererEventListener)};
//                }
//            };
            aacMp3Player = new SimpleExoPlayer.Builder(this).build();
            return true;
        }catch (Exception e){
            Utils.log("createPlayer error: "+e.getMessage());
            return false;
        }
    }

    public void lockWifi(boolean lock){
        try {
            if (wifiLock == null)
                wifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE))
                        .createWifiLock(WifiManager.WIFI_MODE_FULL, "Erdioo lock Wifi");
            if (lock) {
                wifiLock.acquire();
            } else {
                wifiLock.release();
            }
        }catch (Exception e){
            Utils.log("WIFILOCK: "+e.getMessage());
        }
    }


    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Utils.log("ERROR: "+error.getMessage());
        kirimSinyal("error", error.getMessage());
        lockWifi(false);
        stopForeground(true);
        stopSelf();
    }

    @Override
    public void onIsPlayingChanged(boolean isPlaying) {
        this.isPlaying = isPlaying;
        if(isPlaying){
            kirimSinyal("start","");
            lockWifi(true);
            updateNotifikasi();
        }else{
            kirimSinyal("stop","");
            Utils.log("Stopped");
        }
    }


    @Override
    public void onMetadata(Metadata metadata) {
        if(metadata!=null) {
            Utils.log("onMetadata: "+metadata.toString());
            int jml = metadata.length();
            String judul = "";
            for(int n=0;n<jml;n++){
                Metadata.Entry entry = metadata.get(n);
                if (entry instanceof IcyInfo) {
                    final IcyInfo icyInfo = ((IcyInfo) entry);
                    Log.d("IcyInfo", icyInfo.title);
                    judul = icyInfo.title;
                }
            }
            if(!judulLagu.equals(judul)) {
                judulLagu = judul;
                hasMetadata = true;
                if (judulLagu.length() > 3) {
                    updateNotifikasi();
                    Utils.log("UPDATE METADATA");
                    kirimSinyal("lagu", idRadio + "<:>" + judulLagu);
                } else if (judulLagu.length() < 3) {
                    judulLagu = "";
                    updateNotifikasi();
                    Utils.log("UPDATE METADATA");
                    //kirimSinyal("lagu",judulLagu);
                }
            }
        }else{
            Utils.log("onMetadata: NULL");
        }
    }

//    @Override
//    public void playerMetadata(String s, String s1) {
//        Utils.log("METADATA " + s + ", " + s1);
//        if(s!=null) {
//            if (s.equals("StreamTitle") && s1.length()>3) {
//                judulLagu = s1;
//                updateNotifikasi();
//                Utils.log("UPDATE METADATA");
//                kirimSinyal("lagu",idRadio+"<:>"+judulLagu);
//            }else if(s.equals("StreamTitle") && s1.length()<3 && judulLagu.length()>3){
//                judulLagu = "";
//                updateNotifikasi();
//                Utils.log("UPDATE METADATA");
//                //kirimSinyal("lagu",judulLagu);
//            }
//        }
//
//    }


    public void kirimSinyal(String aksi,String data){
        if(!aksinya.contains(aksi)) {
            aksinya += ","+aksi;
            Utils.log("kirimSinyal: "+ aksi+" > "+data);
            Intent intent = new Intent("ay.erdioo.activity.player");
            intent.putExtra("aksi", aksi);
            intent.putExtra("data", data);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }


    Bitmap bmp;
    void connectingNotif(){
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                manager.createNotificationChannel(new NotificationChannel("erdioo", "Erdioo", NotificationManager.IMPORTANCE_DEFAULT));
        }catch (Exception  e){
            Utils.log(e.getMessage());
        }
        if(radioData.getTwitter() != null && radioData.getTwitter().length()>2){
            Utils.log("connectingNotif have twitter");
            //https://twitter.erdioo.net/avatar/ibnux/normal.jpg
            final String url = "https://twitter.erdioo.net/avatar/" + radioData.getTwitter().replace("@","").trim() + "/bigger.jpg";
            Utils.log(url);

            Utils.startMyTask(new AsyncTask<Object, Integer, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Object... objects) {
                    return  Utils.getBitmapFromURL(url,RadioService.this);
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    Intent i = new Intent(getApplicationContext(), RadioActivity.class);
                    Utils.log("connectingNotif have twitter in async");
                    i.putExtra("dataRadio", radioData.toJson());
                    PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"erdioo");
                    builder.setSmallIcon(R.mipmap.ic_launcher)
                            //.setTicker(lagu)
                            .setContentText("Menyambungkan....")
                            .setContentTitle(radioData.getNama())
                            .setContentIntent(pi)
                            .setOngoing(true)
                            .setAutoCancel(false);

                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) builder.setChannelId("erdioo");
                    //nm.notify(0, builder.build());
                    if (bitmap != null){
                        builder.setLargeIcon(bitmap);
                        bmp = bitmap;
                    }

                    startForeground(1, builder.build());
                }
            },"");
        }else {
            Utils.log("connectingNotif no twitter");
            Intent i = new Intent(getApplicationContext(), RadioActivity.class);
            i.putExtra("dataRadio", radioData.toJson());
            PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"erdioo");
                    builder.setSmallIcon(R.mipmap.ic_launcher)
                    //.setTicker(lagu)
                    .setContentText("Menyambungkan....")
                    .setContentTitle(radioData.getNama())
                    .setContentIntent(pi)
                    .setOngoing(true)
                    .setAutoCancel(false);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) builder.setChannelId("erdioo");
            //nm.notify(0, builder.build());
            startForeground(1, builder.build());
        }
    }

    void updateNotifikasi(){
        String lagu = "";
        if(judulLagu.length()<3)
            lagu = radioData.getGenre();
        else if(radioData.getGenre().length()>3)
            lagu = judulLagu;
        Intent i = new Intent(getApplicationContext(), RadioActivity.class);
        i.putExtra("dataRadio",radioData.toJson());
        Utils.log("updateNotifikasi");
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"erdioo");
        builder.setSmallIcon(R.mipmap.ic_launcher)
                //.setTicker(lagu)
                .setContentText(lagu)
                .setContentTitle(radioData.getNama())
                .setContentIntent(pi)
                .setOngoing(true)
                .setSubText("Erdioo")
                .setAutoCancel(false);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) builder.setChannelId("erdioo");
        if (bmp != null){
            builder.setLargeIcon(bmp);
        }
        //nm.notify(0, builder.build());

        startForeground(1, builder.build());
    }

    @Override
    public void onDestroy() {
        if(aacMp3Player!=null)
            aacMp3Player.stop();
        lockWifi(false);
        stopForeground(true);
        Utils.log("STOP ALL");
        super.onDestroy();
    }
}
