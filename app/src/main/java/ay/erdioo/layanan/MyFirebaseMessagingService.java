package ay.erdioo.layanan;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import ay.erdioo.R;
import ay.erdioo.activity.RadioActivity;
import ay.erdioo.activity.SplashActivity;
import ay.erdioo.data.RadioData;
import ay.erdioo.utils.Utils;

/**
 * Created by asatel on 18/07/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(final RemoteMessage pesan) {
        Utils.log(pesan.toString());
        Utils.log(pesan.getData().toString());
        //Utils.log(pesan.getData().get("pesan"));
        //Utils.log(pesan.getData().get("judul"));



        Bitmap bmp = null;
        if(pesan.getData().containsKey("twitter")){
            //https://twitter.erdioo.net/avatar/ibnux/normal.jpg
            bmp = Utils.getBitmapFromURL("https://twitter.erdioo.net/avatar/" +
                    pesan.getData().get("twitter").replace("@","").trim() + "/bigger.jpg",this);
        }
        NotificationCompat.Builder notificationBuilder;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pendingIntent;
        Intent intent = null;
        if(pesan.getData().containsKey("idradio")) {
            intent = new Intent(this, RadioActivity.class);
            RadioData rd = new RadioData();
            rd.setId(pesan.getData().get("idradio"));
            Utils.log(rd.toJson());
            intent.putExtra("dataRadio", rd.toJson());
        }else if(pesan.getData().containsKey("url")){
            intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("notifikasi","notifikasi");
            intent.putExtra("url", pesan.getData().get("url"));
        }
        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSubText("Erdioo")
                //.setContentInfo("Erdioo")
                .setGroup(pesan.getData().get("idradio"))
                .setTicker(pesan.getData().get("pesan"))
                .setContentTitle(pesan.getData().get("judul"))
                .setContentText(pesan.getData().get("pesan"))
                .setAutoCancel(false)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        if(bmp!=null) notificationBuilder.setLargeIcon(bmp);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(pesan.getData().get("judul"));
        bigTextStyle.bigText(pesan.getData().get("pesan"));

        notificationBuilder.setStyle(bigTextStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) (System.currentTimeMillis() / 1000), notificationBuilder.build());



    }

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        Log.d("ERDIOO push", "Refreshed token: " + refreshedToken);
        getSharedPreferences(Utils.PREFS_NAME,0).edit().putString("push",refreshedToken).apply();
        //sendRegistrationToServer(refreshedToken);
        FirebaseMessaging.getInstance().subscribeToTopic("news");
    }
}
