package ay.erdioo.utils;

/**
 * Created by ibnumaksum on 08/11/16.
 */

public class KeyValueData {
    private String key,value;

    public KeyValueData(String key, String value){
        setKey(key);
        setValue(value);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
