package ay.erdioo.utils;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;


/**
 * Created by ibnumaksum on 3/24/15.
 */
public class HttpGET {

    public static void doGetRequest(final String url, final HttpGETcallback callback, Context cx) {
        Utils.log("GET " + url);
        AndroidNetworking.get(url)
                .addHeaders("Accepted",Utils.ACCEPTED)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        if(callback!=null)
                            callback.httpResultSuccess(response);
                    }

                    @Override
                    public void onError(ANError e) {
                        if(url.startsWith("https:")){
                            doGetRequest(url.replace("https://","http://"),callback,null);
                        }else {
                            e.printStackTrace();
                            Utils.log(e.getMessage());
                            if(callback!=null) callback.httpResultError(e.getMessage(), e.getErrorCode());
                        }
                    }
                });
    }

    public static void batal() {

    }

}
