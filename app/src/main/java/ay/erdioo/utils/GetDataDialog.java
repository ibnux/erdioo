package ay.erdioo.utils;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;

import ay.erdioo.R;

public class GetDataDialog extends Dialog {
    private String url, hasil;
    private ProgressBar progressBar;

    public GetDataDialog(Context c) {
        super(c);
        setContentView(R.layout.dialog_get_data);
    }

    public void geturl(String url) {
        this.url = url;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        show();
        HttpGET.doGetRequest(url, new HttpGETcallback() {
            @Override
            public void httpResultSuccess(String result) {
                hasil = result;
                dismiss();
            }

            @Override
            public void httpResultError(String result, int statusCode) {
                hasil = "Error: " + result;
                dismiss();
            }
        },getContext());
    }

    @Override
    public void onBackPressed() {
        HttpGET.batal();
    }

    public String getHasil() {
        return hasil;
    }
}
