package ay.erdioo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ay.erdioo.BuildConfig;
import ay.erdioo.R;
import ay.erdioo.activity.WebFullActivity;

/**
 * Created by ibnumaksum on 12/11/14.
 */
public class Utils {

    public static String PREFS_NAME = "pengaturan";
    public static String PREFS_NAME_DATA = "pengaturan.data";
    public static Fragment fragment;
    public static String ACCEPTED = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
    public static String REST_API = "https://erdioo.net/api/";
//    public static String apikey = "4ed50067f06445622769f4a322d5b2e0";
//    public static String APIURL = "https://api.erdioo.net/json/?key="+apikey+"&";

    public static boolean isLogin = true;
    private static int iklan = 99;

    private static String[] adUnit = new String[]{
        "ca-app-pub-3823597973413223/4963353590",//ibnux
        "ca-app-pub-7772083579939755/1830864424",//bayu
        "ca-app-pub-6624164792217648/2331752603",
        "ca-app-pub-6624164792217648/2331752603",
        "ca-app-pub-6624164792217648/2331752603"
    };

    private static String[] appIds = new String[]{
            "ca-app-pub-3823597973413223~3486620392",//ibnux
            "ca-app-pub-7772083579939755~9354131222",//bayu
            "ca-app-pub-6624164792217648/2331752603",
            "ca-app-pub-6624164792217648/2331752603",
            "ca-app-pub-6624164792217648/2331752603"
    };


    private static String[] adUnitInter = new String[]{
            "ca-app-pub-3823597973413223/7916819993",
            "ca-app-pub-6624164792217648/3808485800",
            "ca-app-pub-6624164792217648/3808485800",
            "ca-app-pub-6624164792217648/3808485800",
            "ca-app-pub-7772083579939755/6065600821"
    };

    private static String[] news = new String[]{
            "https://lapi.kumparan.com/v2.0/rss/"
    };

    public static String getNewsRss(){
        return news[new Random().nextInt(news.length)];
    }

    public static AdView getIklanGoogle(Context c) {
        return getIklanGoogle(c,"");
    }

    public static AdView getIklanGoogle(Context c, String keyword){
        try {
            AdView mAdView = new AdView(c);
            mAdView.setAdSize(AdSize.BANNER);
            if(iklan==99) iklan = new Random().nextInt(adUnit.length);
            String adunit = adUnit[iklan];
            if(BuildConfig.DEBUG) adunit = "ca-app-pub-3940256099942544/6300978111";
            String appid = appIds[iklan];
            log("iklan: " + iklan + " "+adunit+" "+appid);
            try {
                ApplicationInfo ai = c.getPackageManager().getApplicationInfo(c.getPackageName(), PackageManager.GET_META_DATA);
                Bundle bundle = ai.metaData;
                String myApiKey = bundle.getString("com.google.android.gms.ads.APPLICATION_ID");
                log("APPLICATION_ID: " + myApiKey);
                ai.metaData.putString("com.google.android.gms.ads.APPLICATION_ID", appid);//you can replace your key APPLICATION_ID here
                String ApiKey = bundle.getString("com.google.android.gms.ads.APPLICATION_ID");
                log("ReNamed v: " + ApiKey);
            } catch (PackageManager.NameNotFoundException e) {
                log("Failed to load meta-data, NameNotFound: " + e.getMessage());
            } catch (NullPointerException e) {
                log("Failed to load meta-data, NullPointer: " + e.getMessage());
            }
            MobileAds.initialize(c);
            mAdView.setAdUnitId(adunit);
            mAdView.setLayoutParams(new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER_HORIZONTAL | Gravity.TOP));
            AdRequest adRequest = getAdRequest(keyword);
            mAdView.loadAd(adRequest);
            log("getIklanGoogle : "+adunit +" "+appid);
            return mAdView;
        }catch (Exception e){
            e.printStackTrace();
            log("getIklanGoogle Error "+e.getMessage());
            return null;
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static AdRequest getAdRequest(String keyword){
        AdRequest.Builder adRequest = new AdRequest.Builder();
        if(!keyword.isEmpty())
            adRequest.addKeyword(keyword);

        return adRequest.build();
    }

    public static String getAdUnitIDInter(){
        String ad = "ca-app-pub-3940256099942544/1033173712";
        if(iklan==99) iklan = new Random().nextInt(adUnitInter.length);
        if(!BuildConfig.DEBUG)
            ad = adUnitInter[iklan];
        Utils.log("getAdUnitIDInter "+ad);
        return ad;
    }

    public static void showToast(String pesan, Context c) {
        try {
            Toast.makeText(c, pesan, Toast.LENGTH_LONG).show();
        }catch (Exception e){
            log(e.toString());
        }

    }

    private static final String PICASSO_CACHE = "picasso-cache";

    public static void clearCache(Context context) {
        final File cache = new File(
                context.getApplicationContext().getCacheDir(),
                PICASSO_CACHE);
        if (cache.exists()) {
            deleteFolder(cache);
        }
    }

    public static double getZonaWaktu() {
        Calendar now = new GregorianCalendar();
        int gmtOffset = now.getTimeZone().getOffset(now.getTimeInMillis());
        return gmtOffset / 3600000;
    }

    public static Bitmap getScaledBitmap(String path, int newSize) {
        File image = new File(path);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inInputShareable = true;
        options.inPurgeable = true;

        BitmapFactory.decodeFile(image.getPath(), options);
        if ((options.outWidth == -1) || (options.outHeight == -1))
            return null;

        int originalSize = (options.outHeight > options.outWidth) ? options.outHeight
                : options.outWidth;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / newSize;

        Bitmap scaledBitmap = BitmapFactory.decodeFile(image.getPath(), opts);
        return scaledBitmap;
    }

    private static void deleteFolder(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteFolder(child);
        }
        fileOrDirectory.delete();
    }

    public static ArrayList getLinks(String text) {
        ArrayList links = new ArrayList();

        String regex = "\\(?\\b(https?://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            links.add(urlStr);
        }
        return links;
    }

    public static String getBulan(int month) {
        month++;
        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sep";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";
            default:
                return "Invalid";
        }
    }



    public static String milisToTanggal(long waktu) {
        return milisToTanggal(waktu, false);
    }

    public static String getRealPathFromURI(Uri contentUri, Context c) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = c.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }


    public static String milisToTanggal(long waktu, boolean bulan) {
        return milisToTanggal(waktu, false,true);
    }

    public static String milisToTanggal(long waktu, boolean bulan, boolean usetahun ) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(waktu);
        String tahun = "";
        if(usetahun) tahun = cal.get(Calendar.YEAR) + " ";
        if (bulan)
            return cal.get(Calendar.DAY_OF_MONTH) + " " + getBulan(cal.get(Calendar.MONTH)) + " " +
                    tahun + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        else
            return cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" +
                    tahun + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
    }


    public static void log(String txt) {
        Log.d("ERDIOO", "------------------------------------------------------");
        Log.d("ERDIOO", txt + " ");
        Log.d("ERDIOO", "------------------------------------------------------");
    }

    public static void deleteDirectoryTree(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteDirectoryTree(child);
            }
        }

        fileOrDirectory.delete();
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static long strToTime(String tanggal){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return dateFormat.parse(tanggal).getTime();
        } catch (ParseException e) {
            log("strToTime :" +tanggal +" "+e.toString());
        }
        return System.currentTimeMillis();
    }


    public static String time_offset(long waktu, Context ct) {
        return time_offset(waktu, true,true,true, ct);
    }

    public static String time_offset(long waktu, boolean tanggal, Context ct) {
        return time_offset(waktu, tanggal,true,true, ct);
    }
    public static String time_offset(long waktu, boolean tanggal,boolean useBulan, boolean useTahun, Context ct) {
        long o = (System.currentTimeMillis() / 1000) - (waktu / 1000);
        if (o < 0) {
            o = (waktu / 1000) - (System.currentTimeMillis() / 1000);
            if (o <= 1) return ct.getString(R.string.barusan);
            if (o < 20)
                return o + " " + ct.getString(R.string.detik) + " " + ct.getString(R.string.lagi);
            if (o < 40)
                return "1/2 " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lagi);
            if (o < 60)
                return "1/2 " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lagi);
            if (o <= 90)
                return "1 " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lagi);
            if (o <= 59 * 60)
                return Math.round(o / 60) + " " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lagi);
            if (o <= 60 * 60 * 1.5)
                return "1 " + ct.getString(R.string.jam) + " " + ct.getString(R.string.lagi);
            if (o <= 60 * 60 * 24)
                return Math.round(o / 60 / 60) + " " + ct.getString(R.string.jam) + " " + ct.getString(R.string.lagi);
            if (o <= 60 * 60 * 24 * 1.5)
                return "1 " + ct.getString(R.string.hari) + " " + ct.getString(R.string.lagi);
            if (o < 60 * 60 * 24 * 7)
                return Math.round(o / 60 / 60 / 24) + " " + ct.getString(R.string.hari) + " " + ct.getString(R.string.lagi);
            if (o <= 60 * 60 * 24 * 9)
                return "1 " + ct.getString(R.string.minggu) + " " + ct.getString(R.string.lagi);
            if (o > 60 * 60 * 24 * 9)
                return Math.round(o / 60 / 60 / 24 / 7) + "  " + ct.getString(R.string.minggu) + " " + ct.getString(R.string.lagi);
        } else {
            if (o <= 1) return ct.getString(R.string.barusan);
            if (o < 20)
                return o + " " + ct.getString(R.string.detik) + " " + ct.getString(R.string.lalu);
            if (o < 40)
                return "1/2 " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lalu);
            if (o < 60)
                return "1/2 " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lalu);
            if (o <= 90)
                return "1 " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lalu);
            if (o <= 59 * 60)
                return Math.round(o / 60) + " " + ct.getString(R.string.menit) + " " + ct.getString(R.string.lalu);
            if (o <= 60 * 60 * 1.5)
                return "1 " + ct.getString(R.string.jam) + " " + ct.getString(R.string.lalu);
            if (o <= 60 * 60 * 24)
                return Math.round(o / 60 / 60) + " " + ct.getString(R.string.jam) + " " + ct.getString(R.string.lalu);
            if (o <= 60 * 60 * 24 * 1.5)
                return "1 " + ct.getString(R.string.hari) + " " + ct.getString(R.string.lalu);
            if (o < 60 * 60 * 24 * 30)
                return Math.round(o / 60 / 60 / 24) + " " + ct.getString(R.string.hari) + " " + ct.getString(R.string.lalu);
        }
        if (tanggal)
            return milisToTanggal(waktu, useBulan, useTahun);
        else
            return "";
    }

    public static Bitmap getBitmapFromURL(String src, Context c) {
        try {
            String md5 = MD5(src);
            File file = new File(c.getCacheDir(), md5);
            if(file.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getPath());
                return myBitmap;
            }else {
                java.net.URL url = new java.net.URL(src);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                myBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(bos.toByteArray());
                fos.flush();
                fos.close();
                return myBitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void startMyTask(AsyncTask asyncTask, Object... params) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        else
            asyncTask.execute(params);
    }

    public static final String MD5(final String s) {
        log("MD5:"+s);
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void postTweet(String txt, Context cx){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/intent/tweet")
                    .buildUpon()
                    .appendQueryParameter("text",txt)
                    .build());
            cx.startActivity(intent);
        }catch (Exception e){}
    }

    public static void bukaBrowser(String url, Activity activity){
        if(Build.VERSION.SDK_INT >= 15) {
            String PACKAGE_NAME = "com.android.chrome";

            CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                    .setToolbarColor(Color.parseColor("#1c73a5"))
                    .setShowTitle(true)
                    .build();
            customTabsIntent.intent.setData(Uri.parse(url));

            PackageManager packageManager = activity.getPackageManager();
            List<ResolveInfo> resolveInfoList = packageManager.queryIntentActivities(customTabsIntent.intent, PackageManager.MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resolveInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                if (TextUtils.equals(packageName, PACKAGE_NAME))
                    customTabsIntent.intent.setPackage(PACKAGE_NAME);
            }

            customTabsIntent.launchUrl(activity, Uri.parse(url));
        }else{
            Intent intent = new Intent(activity, WebFullActivity.class);
            intent.putExtra("link", url);
            activity.startActivity(intent);
        }
    }

}
