package ay.erdioo.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;

import ay.erdioo.MainActivity;
import ay.erdioo.R;
import ay.erdioo.data.RadioData;
import ay.erdioo.utils.Utils;

public class SplashActivity extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(mainIntent);
                try {
                    final Intent intent = getIntent();
                    if(intent.hasExtra("notifikasi")){
                        if(intent.hasExtra("idradio")){
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent(SplashActivity.this, RadioActivity.class);
                                    RadioData rd = new RadioData();
                                    rd.setId(intent.getStringExtra("idradio"));
                                    i.putExtra("dataRadio", rd.toJson());
                                    startActivity(i);
                                    finish();
                                }
                            }, 1000);
                        }else if(intent.hasExtra("url")){
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent(SplashActivity.this, KeTkpActivity.class);
                                    i.putExtra("url", intent.getStringExtra("url"));
                                    startActivity(i);
                                    finish();
                                }
                            }, 1000);
                        }else
                            finish();
                    }else {
                        Utils.log(intent.getData().toString());
                        Uri uri = Uri.parse(intent.getData().toString());
                        Utils.log("getPath " + uri.getPath());
                        Utils.log("getQuery " + uri.getQuery());
                        Utils.log("getQuery " + uri.getQueryParameter("asik"));
                        final Intent i;
                        //https://erdioo.net/radio/833/angling-darma-fm-tulungagung.html
                        if (uri.getPath().startsWith("/radio/")) {
                            String id = uri.getPath().substring(7, uri.getPath().lastIndexOf("/"));
                            Utils.log("ID Radio " + uri.getPath() + " " + id);
                            i = new Intent(SplashActivity.this, RadioActivity.class);
                            RadioData rd = new RadioData();
                            rd.setId(id);
                            Utils.log(rd.toJson());
                            i.putExtra("dataRadio", rd.toJson());
                        } else if (uri.getPath().startsWith("/propinsi/")) {
                            String pt = uri.getPath().substring(10);
                            Utils.log(uri.getPath() + " " + pt);
                            String[] pts = pt.split("/");
                            i = new Intent(SplashActivity.this, PropinsiActivity.class);
                            i.putExtra("idPropinsi", pts[0]);
                            i.putExtra("namaPropinsi", pts[1].replace(".html", "").replace("-", " ").toUpperCase());
                        } else {
                            i = new Intent(SplashActivity.this, KeTkpActivity.class);
                            i.putExtra("url", intent.getData().toString());
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(i);
                                finish();
                            }
                        }, 1000);
                    }
                }catch (Exception e){
                    Utils.log("no segment");
                    finish();
                }
                //finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_blank, menu);
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
