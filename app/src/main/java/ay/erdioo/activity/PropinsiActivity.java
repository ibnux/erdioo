package ay.erdioo.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import ay.erdioo.R;
import ay.erdioo.data.RadioData;
import ay.erdioo.utils.HttpGET;
import ay.erdioo.utils.HttpGETcallback;
import ay.erdioo.utils.RoundedTransformation;
import ay.erdioo.utils.Utils;

public class PropinsiActivity extends Activity implements HttpGETcallback {
    String idPropinsi,namaPropinsi;
    private ListView listView;
    private SwipeRefreshLayout swipe;
    ArrayAdapter adapter;
    EditText txtcari;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propinsi);
        Intent i = getIntent();
        idPropinsi = i.getStringExtra("idPropinsi");
        namaPropinsi = i.getStringExtra("namaPropinsi");
        TextView textViewPropinsi = (TextView)findViewById(R.id.namaPropinsi);
        textViewPropinsi.setText(namaPropinsi);
        swipe = (SwipeRefreshLayout) findViewById(R.id.listViewSwipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HttpGET.doGetRequest(Utils.REST_API + "radio/propinsi/" + idPropinsi, PropinsiActivity.this, PropinsiActivity.this);
                swipe.setRefreshing(true);
            }
        });
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RadioData radio = (RadioData) listView.getAdapter().getItem(position);
                if(radio!=null){
                    Intent intent = new Intent(PropinsiActivity.this, RadioActivity.class);
                    intent.putExtra("dataRadio", radio.toJson());
                    startActivity(intent);
                }
            }
        });

        txtcari = (EditText) findViewById(R.id.editTextCari);
        txtcari.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                PropinsiActivity.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {}
        });

        swipe.setRefreshing(true);
        SharedPreferences settings = getSharedPreferences(Utils.PREFS_NAME, 0);
        String json = settings.getString("propinsi"+idPropinsi, "[]");
        long now = (System.currentTimeMillis() / 1000) - (settings.getLong("propinsiDate"+idPropinsi, 0) / 1000);
        long lama = 2* 24 * 60 * 60;
        Utils.log(now + " > " + lama);
        if (json.length() > 5) {
            httpResultSuccess(json);
        }

        if (now > lama) {
            HttpGET.doGetRequest(Utils.REST_API + "radio/propinsi/" + idPropinsi, this,this );
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("propinsi"+idPropinsi, "");
            editor.putLong("propinsiDate"+idPropinsi, 0);
            editor.apply();
        }

        //Iklan Google
        mAdView = Utils.getIklanGoogle(this);
        ((LinearLayout)findViewById(R.id.erdiooad)).addView(mAdView);


        Bundle params = new Bundle();
        params.putString("id_propinsi", idPropinsi);
        params.putString("nama_propinsi", namaPropinsi);
        FirebaseAnalytics.getInstance(this).logEvent("buka_propinsi_"+idPropinsi,params);
    }

    AdView mAdView;

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_blank, menu);
        return true;
    }

    @Override
    public void httpResultSuccess(String result) {
        try{
            JSONArray jArray = new JSONArray(result);
            int jm = jArray.length();
            if(jm>25) txtcari.setVisibility(View.VISIBLE);
            RadioData[] radioDatas = new RadioData[jm];
            for (int n = 0; n < jm; n++) {
                JSONObject obj = jArray.getJSONObject(n);
                radioDatas[n] = new RadioData(obj.getString("IdRadio"),
                        obj.getString("NamaRadio"),
                        obj.getString("Lokasi"),
                        obj.getString("Genre"),
                        obj.getString("Twitter"));
            }

            adapter = new ArrayAdapter<RadioData>(
                    getBaseContext(),
                    R.layout.item_menu_duatext_satugambar,
                    R.id.textBesar,
                    radioDatas
            ) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = convertView;
                    ViewHolder holder;
                    if(view==null) {
                        LayoutInflater vi = (LayoutInflater) getContext()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = vi.inflate(R.layout.item_menu_duatext_satugambar, null);
                        //View view = super.getView(position, convertView, parent);
                        holder = new ViewHolder();
                        holder.nama = (TextView) view.findViewById(R.id.textBesar);
                        holder. genre = (TextView) view.findViewById(R.id.textKecil);
                        holder. icon = (ImageView) view.findViewById(R.id.icon);
                        view.setTag(holder);
                    }else
                        holder = (ViewHolder)view.getTag();

                    RadioData radio = getItem(position);
                    holder.nama.setText(radio.getNama());
                    holder.genre.setText(radio.getGenre());
                    if (radio.getTwitter().length() > 4)
                        Picasso.with(getApplicationContext())
                                .load("https://twitter.erdioo.net/avatar/" + radio.getTwitter() + "/bigger.jpg")
                                .fit()
                                .placeholder(R.drawable.ic_launcher)
                                .error(R.drawable.ic_launcher)
                                .transform(new RoundedTransformation(4, 0))
                                .into(holder.icon);
                    return view;
                }
            };

            listView.setAdapter(adapter);
            SharedPreferences settings = getSharedPreferences(Utils.PREFS_NAME, 0);
            if (!settings.getString("propinsi"+idPropinsi, "[]").equals(result)) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("propinsi"+idPropinsi, result);
                editor.putLong("propinsiDate"+idPropinsi, System.currentTimeMillis());
                editor.apply();
            }
        }catch (Exception e){
            Utils.log("ERROR "+e.getMessage());
        }
        swipe.setRefreshing(false);
    }
    private static class ViewHolder{
        public TextView nama,genre;
        public ImageView icon;
    }

    @Override
    public void httpResultError(String result, int statusCode) {
        Utils.showToast("Gagal mengambil data Propinsi", getApplication());
        swipe.setRefreshing(false);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

}
