package ay.erdioo.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import ay.erdioo.R;
import ay.erdioo.RadioApp;
import ay.erdioo.data.RadioData;
import ay.erdioo.fragment.ChatFragment;
import ay.erdioo.fragment.RadioHomeFragment;
import ay.erdioo.fragment.RadioTweetsFragment;
import ay.erdioo.layanan.RadioService;
import ay.erdioo.utils.GetDataDialog;
import ay.erdioo.utils.RoundedTransformation;
import ay.erdioo.utils.Utils;

public class RadioActivity extends FragmentActivity {
    RadioData radioData = new RadioData();
    int widthSize,heightSize;
    TextView textViewJudul,textnama,textgenre;
    String aksinya="",link="";
    public ImageView logo,cover,imagePlay;
    private Handler handler = new Handler();
    CardView layoutRadio;

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    private String[] menuAtas;

    LinearLayout loadingProgress;

    boolean isConnecting = false;

    LinearLayout erdiooad;
    AdView mAdView;

    public static boolean isAd = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        erdiooad = (LinearLayout) findViewById(R.id.erdiooad);
        loadingProgress = (LinearLayout) findViewById(R.id.loadingProgress);
        if(isAd) loadingProgress.setVisibility(View.GONE);
        Intent intent = getIntent();
        Utils.log(intent.getStringExtra("dataRadio"));
        radioData.setJson(intent.getStringExtra("dataRadio"));
        link = "https://erdioo.net/radio/"+radioData.getId()+"/player.html";

        cover = (ImageView) findViewById(R.id.imageViewCover);
        logo = (ImageView) findViewById(R.id.imageViewLogo);
        textnama = (TextView) findViewById(R.id.textViewNama);
        textgenre = (TextView) findViewById(R.id.textViewGenre);
        layoutRadio = (CardView) findViewById(R.id.layoutRadio);
        imagePlay = findViewById(R.id.imagePlay);

        textViewJudul = (TextView) findViewById(R.id.textViewJudul);
        textnama.setText(radioData.getNama());
        textnama.setSelected(true);
        textgenre.setText(radioData.getGenre());
        textgenre.setSelected(true);
        if(RadioApp.nowRadio!=null) {
            if (RadioApp.nowRadio.getId().equals(radioData.getId())) {
                Utils.log("Radio masih sama");
                radioData = RadioApp.nowRadio;
                textViewJudul.setText(radioData.getNama());
                textViewJudul.setVisibility(View.VISIBLE);
                loadingProgress.setVisibility(View.GONE);
                if(RadioService.isPlaying)
                    imagePlay.setImageResource(R.drawable.ic_action_stop);
                else
                    imagePlay.setImageResource(R.drawable.ic_action_play);
            }else{
                Utils.log("Radio beda");
                berhentikanRadio();
            }
        }

        hitungTinggi(cover);



        /*Picasso.with(this)
                .load("https://api.erdioo.net/splash/splashiklan.png")
                .fit()
                .transform(new RoundedTransformation(4, 0))
                .into(iklan);*/



        Utils.log("URL "+radioData.getUrlStreaming());
        if(radioData.getUrlStreaming().length()<4) {
            final GetDataDialog dialogGet = new GetDataDialog(RadioActivity.this);
            dialogGet.setTitle("Mengambil data radio...");
            dialogGet.setCanceledOnTouchOutside(false);
            dialogGet.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    try{
                        JSONObject json = new JSONObject(dialogGet.getHasil());
                        radioData.setId(json.getString("IdRadio"));
                        radioData.setNama(json.getString("NamaRadio"));
                        radioData.setGenre(json.getString("Genre"));
                        radioData.setTwitter(json.getString("Twitter"));
                        radioData.setUrlStreaming(json.getString("URLStreaming"));
                        radioData.setFormat(json.getString("Format"));
                        radioData.setAdUnit(json.getString("AdUnit"));
                        radioData.setWebsite(json.getString("Website"));
                        radioData.setRss(json.getString("Rss"));
                        radioData.setTelepon(json.getString("Telepon"));
                        mulaiPlayer();
                    }catch (Exception e){
                        Utils.showToast("Gagal mengambil data", getApplication());
                        Utils.log(e.toString());
                        finish();
                    }
                }
            });
            dialogGet.geturl(Utils.REST_API + "radio/"+radioData.getId());
        }else{
            mulaiPlayer();
        }


        imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(RadioService.isPlaying){
                    berhentikanRadio();
                    imagePlay.setImageResource(R.drawable.ic_action_play);
                }else{
                    jalankanRadio();
                    imagePlay.setImageResource(R.drawable.ic_action_stop);
                }
            }
        });

    }



    public void pasangIklanRadio(String adUnit) {
        mAdView = Utils.getIklanGoogle(this,radioData.getNama());
        erdiooad.addView(mAdView);
    }



    //Timed Out Radio
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            berhentikanRadio();
            Intent intent = new Intent("ay.erdioo.activity.player");
            intent.putExtra("aksi", "error");
            intent.putExtra("data", "");
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    };

    public void berhentikanRadio(){
        Intent i = new Intent(getApplicationContext(), RadioService.class);
        i.setAction(RadioService.ACTION_STOP);
        startService(i);
    }

    public void jalankanRadio(){
        Intent i = new Intent(this, RadioService.class);
        i.putExtra("dataRadio", radioData.toJson());
        i.putExtra("idRadio", radioData.getId());
        i.setAction(RadioService.ACTION_PLAY);
        startService(i);
        Utils.log("GENRE " + radioData.getGenre());
    }




    public void mulaiPlayer(){
        if(!radioData.getGenre().toLowerCase().contains("religi"))
            pasangIklanRadio(radioData.getAdUnit());
        Bundle params = new Bundle();
        params.putString("id_radio", radioData.getId());
        params.putString("nama_radio", radioData.getNama());
        FirebaseAnalytics.getInstance(this).logEvent("listen_radio_"+radioData.getId(),params);
        textnama.setText(radioData.getNama());
        textnama.setSelected(true);
        textgenre.setText(radioData.getGenre());
        textgenre.setSelected(true);
        Picasso.with(this)
                .load("https://twitter.erdioo.net/cover/?name=" + radioData.getTwitter())
                .fit()
                .placeholder(R.drawable.coverradio)
                .error(R.drawable.coverradio)
                .into(cover);
        Picasso.with(this)
                .load("https://twitter.erdioo.net/avatar/" + radioData.getTwitter() + "/bigger.jpg")
                .fit()
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .transform(new RoundedTransformation(4, 0))
                .into(logo);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0)
                    layoutRadio.setVisibility(View.VISIBLE);
                else
                    layoutRadio.setVisibility(View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if(!isAd)
            jalankanRadio();
    }

    void hitungTinggi(ImageView cover){
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int imageHeight = 500;
        int imageWidth = 1500;

        widthSize = displayMetrics.widthPixels;
        heightSize = displayMetrics.heightPixels;

        float imageRatio = 0.0F;
        if (imageHeight > 0) {
            imageRatio = imageWidth / imageHeight;
        }
        float sizeRatio = 0.0F;
        if (heightSize > 0) {
            sizeRatio = widthSize / heightSize;
        }

        int width;
        int height;
        if (imageRatio >= sizeRatio) {
            // set width to maximum allowed
            width = widthSize;
            // scale height
            height = width * imageHeight / imageWidth;
        } else {
            // set height to maximum allowed
            height = heightSize;
            // scale width
            width = height * imageWidth / imageHeight;
        }
        cover.getLayoutParams().height = height;
        cover.getLayoutParams().width = width;
        cover.requestLayout();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation change
        if (newConfig.orientation != Configuration.ORIENTATION_LANDSCAPE) {
            super.onConfigurationChanged(newConfig);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_radio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        stopMpe();
        handler.removeCallbacks(runnable);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(respon);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playerReceiver);
        super.onDestroy();
    }

    ResponseReceiver respon;

    @Override
    protected void onPause() {
        stopMpe();
        if (mAdView != null) {
            mAdView.pause();
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(respon);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playerReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        Utils.log("Radio onResume");
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
        startListener();
    }

    @Override
    protected void onStop() {
        stopMpe();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(respon);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playerReceiver);
        super.onStop();
    }

    @Override
    protected void onStart() {
        startListener();
        super.onStart();
    }

    private void startListener() {
        respon = new ResponseReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(respon, new IntentFilter("ay.erdioo.activity.player"));
        LocalBroadcastManager.getInstance(this).registerReceiver(playerReceiver, new IntentFilter("ay.erdioo.activity.playerReceiver"));
    }

    MediaPlayer mpe;

    public void stopMpe(){
        if(mpe!=null)
            if(mpe.isPlaying())
                mpe.stop();
    }

    private class ResponseReceiver extends BroadcastReceiver {
        public ResponseReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Utils.log("BROADCAST RECEIVED:"+intent.getStringExtra("aksi")+"-:-"+intent.getStringExtra("data"));
                String aksi = intent.getStringExtra("aksi");
                if(!aksinya.contains(aksi)) {
                    if(!aksi.equals("lagu"))
                        aksinya += "," + aksi;
                    if (aksi.equals("error")) {
                        imagePlay.setImageResource(R.drawable.ic_action_play);
                        stopMpe();
                        //Utils.showToast(intent.getStringExtra("data"), context);
                        Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        // Vibrate for 500 milliseconds
                        v.vibrate(1000);
                        new AlertDialog.Builder(RadioActivity.this)
                                .setIcon(R.mipmap.ic_launcher)
                                .setTitle(R.string.app_name)
                                .setMessage("Gagal mendapatkan sinyal dari stasiun radio.\n" +
                                        "kemungkinan server sedang tidak aktif.\n\n" +
                                        "Beritahu @erdioo agar diupdate informasi server radionya, jika ini terus terjadi.\n\n" +
                                        intent.getStringExtra("data"))
                                .setPositiveButton("Ok", null)
                                .setNeutralButton("Kasih tahu Radionya?", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(radioData.getTwitter().length()>3) {
                                            String tweet = "Dear " + radioData.getNama() + " (@" + radioData.getTwitter() + ") di @Erdioo sudah tidak bisa diakses nih :(";
                                            Utils.postTweet(tweet, RadioActivity.this);
                                        }else{
                                            Utils.showToast("Radionya ngga punya Twitter :(",RadioActivity.this);
                                        }
                                    }
                                })
                                .setNegativeButton("Kasih tahu Erdioo?", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(radioData.getTwitter().length()>3) {
                                            String tweet = "Dear @Erdioo, Radio " + radioData.getNama() + " (@" + radioData.getTwitter() + ") sudah tidak bisa diakses nih :(";
                                            Utils.postTweet(tweet, RadioActivity.this);
                                        }else{
                                            String tweet = "Dear @Erdioo, Radio " + radioData.getNama() + " sudah tidak bisa diakses nih :(";
                                            Utils.postTweet(tweet, RadioActivity.this);
                                        }
                                    }
                                })
                                .setCancelable(false)
                                .show();
                    } else if (aksi.equals("connecting")) {
                        handler.postDelayed(runnable, 30000);
                        isConnecting = true;
                        mpe = MediaPlayer.create(RadioActivity.this, R.raw.tuning);
                        mpe.setLooping(true);
                        mpe.start();
                    } else if (aksi.equals("stop")) {
                        isConnecting = false;
                    } else if (aksi.equals("start")) {
                        imagePlay.setImageResource(R.drawable.ic_action_stop);
                        RadioApp.nowRadio = radioData;
                        handler.removeCallbacks(runnable);
                        isConnecting = false;
                        loadingProgress.setVisibility(View.GONE);
                        stopMpe();
                    } else if (aksi.equals("lagu")) {
                        RadioApp.nowRadio = radioData;
                        String[] tmp = intent.getStringExtra("data").split("<:>");
                        if (tmp[0].equals(radioData.getId())) {
                            textViewJudul.setText(tmp[1]);
                            textViewJudul.setVisibility(View.VISIBLE);
                            textViewJudul.setSelected(true);
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    BroadcastReceiver playerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }



    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)  && isTaskRoot()) {


            if (getSharedPreferences(Utils.PREFS_NAME, 0).getBoolean("sudahRate", false)) {
                new AlertDialog.Builder(this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.dialog_tanya_tutup)
                        .setNegativeButton(R.string.dialog_matikan_aplikasi, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                berhentikanRadio();
                                finish();
                            }

                        })
                        .setPositiveButton(R.string.dialog_tutup_aplikasi, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .show();
            } else {
                new AlertDialog.Builder(this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.dialog_tanya_tutup)
                        .setNegativeButton(R.string.dialog_matikan_aplikasi, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                berhentikanRadio();
                                finish();
                            }

                        })
                        .setPositiveButton(R.string.dialog_tutup_aplikasi, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNeutralButton(R.string.dialog_rate_aplikasi, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getSharedPreferences(Utils.PREFS_NAME, 0).edit().putBoolean("sudahRate", true).commit();
                                try {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse("market://details?id=ay.erdioo"));
                                    startActivity(intent);

                                } catch (Exception e) {
                                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=ay.erdioo"));
                                    startActivity(i);
                                }
                                finish();
                            }
                        })
                        .show();
            }
            return true;

        }

        if(keyCode == KeyEvent.KEYCODE_BACK && isConnecting){
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle(R.string.app_name)
                    .setMessage("Aplikasi masih mencoba melakukan koneksi ke server. yakin mau ditutup?")
                    .setPositiveButton("Yakin", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            berhentikanRadio();
                            finish();
                        }

                    })
                    .setNegativeButton("Gak Yakin",null)
                    .setNeutralButton("Ragu ragu", null)
                    .show();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
    RadioHomeFragment radioHomeFragment;
    ChatFragment chatFragment;
    RadioTweetsFragment radioTweetsFragment;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int pos) {
            if (pos == 0) {
                if (radioHomeFragment==null) radioHomeFragment = RadioHomeFragment.newInstance(pos + 1, radioData.toJson());
                return radioHomeFragment;
            }else if (pos == 1){
                if (chatFragment==null) chatFragment = ChatFragment.newInstance(pos + 1, radioData.toJson());
                return chatFragment;
            }else if (pos == 2){
                if (radioTweetsFragment==null) radioTweetsFragment = RadioTweetsFragment.newInstance(pos + 1, radioData.toJson());
                return radioTweetsFragment;
            }

            return PlaceholderFragment.newInstance(pos + 1);
        }

        @Override
        public int getCount() {
            if (menuAtas == null)
                menuAtas = getResources().getStringArray(R.array.menu_radio);
            return menuAtas.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (menuAtas == null)
                menuAtas = getResources().getStringArray(R.array.menu_radio);
            return menuAtas[position];
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
}
