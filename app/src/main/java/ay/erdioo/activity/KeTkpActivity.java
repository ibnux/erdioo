/*
 * Dibuat pada 19/9/2016
 * Creator: Ibnu Maksum @ibnux
 * Copyright (c) 2016. PT. Asia Sarana Telekomunika
 *
 */

package ay.erdioo.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import ay.erdioo.R;
import ay.erdioo.utils.Utils;


public class KeTkpActivity extends AppCompatActivity {
    WebView myWebView;
    SwipeRefreshLayout swipe;
    Toolbar toolbar;
    private String urlWeb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ke_tkp);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("ke TKP");

        myWebView = (WebView) findViewById(R.id.webViewTKP);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe.setRefreshing(true);
                        myWebView.reload();
                    }
                });
            }
        });
        Intent intent = getIntent();
        if (intent.hasExtra("url"))
            this.urlWeb = intent.getStringExtra("url");
        else finish();

        FirebaseAnalytics.getInstance(this).logEvent("ke_tkp",null);
        new WebViewTask().execute();
    }

    private class WebViewTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Void... param) {
                        /* this is very important - THIS IS THE HACK */
            SystemClock.sleep(1000);
            return false;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            WebSettings webSettings = myWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setBuiltInZoomControls(false);
            webSettings.setAppCacheEnabled(true);
            myWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.startsWith("http")) {
                        // This is my web site, so do not override; let my WebView load the page
                        return false;
                    }
                    // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    return true;
                }



                @Override
                public void onPageFinished(WebView view, String url) {
                    swipe.setRefreshing(false);
                    try{
                        toolbar.setTitle(view.getTitle());
                        toolbar.setSubtitle(url);
                        //toolbar.setLogo(new BitmapDrawable(getResources(),view.getFavicon()));
                    }catch (Exception e){
                        Utils.log(e.toString());
                    }
                    super.onPageFinished(view, url);
                }



                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    swipe.post(new Runnable() {
                        @Override
                        public void run() {
                            swipe.setRefreshing(true);
                        }
                    });

                    try{
                        toolbar.setTitle(view.getTitle());
                        toolbar.setSubtitle(url);
                    }catch (Exception e){
                        Utils.log(e.toString());
                    }
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    view.loadUrl("about:blank");
                    Utils.showToast("Failed to get data from server",KeTkpActivity.this);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(view.getUrl()));
                    startActivity(intent);
                }

                @Override
                public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(KeTkpActivity.this);
                    builder.setMessage("Keamanan Website tidak terjamin, Sertifikat SSL website yg dituju berbeda ");
                    builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.proceed();
                        }
                    });
                    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.cancel();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            myWebView.loadUrl(urlWeb);
        }
    }

    @Override
    public void onBackPressed() {
        if(myWebView.canGoBack()){
            swipe.post(new Runnable() {
                @Override
                public void run() {
                    swipe.setRefreshing(true);
                    myWebView.goBack();
                }
            });
        }else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_ketkp, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_open:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myWebView.getUrl()));
                startActivity(intent);
                return true;

            case R.id.action_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, myWebView.getTitle()+" "+myWebView.getUrl());
                startActivity(Intent.createChooser(sharingIntent,"Share using"));
                return true;
            case R.id.action_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
