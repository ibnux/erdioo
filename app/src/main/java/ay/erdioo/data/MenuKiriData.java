package ay.erdioo.data;

/**
 * Created by ibnumaksum on 12/11/14.
 */
public class MenuKiriData {
    private String ikon, teks;

    public MenuKiriData(String ikon, String teks) {
        setIkon(ikon);
        setTeks(teks);
    }

    public void setIkon(String ikon) {
        this.ikon = ikon;
    }

    public void setTeks(String teks) {
        this.teks = teks;
    }

    public String getIkon() {
        return ikon;
    }

    public String getTeks() {
        return teks;
    }
}

