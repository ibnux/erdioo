package ay.erdioo.data;

/**
 * Created by ibnumaksum on 4/23/15.
 */
public class PropinsiData {
    private String idPropinsi="",NamaPropinsi="",JumlahRadio="";

    public PropinsiData(String idPropinsi,String NamaPropinsi,String JumlahRadio){
        setIdPropinsi(idPropinsi);
        setNamaPropinsi(NamaPropinsi);
        setJumlahRadio(JumlahRadio);
    }

    public void setIdPropinsi(String idPropinsi) {
        this.idPropinsi = idPropinsi;
    }

    public void setJumlahRadio(String jumlahRadio) {
        JumlahRadio = jumlahRadio;
    }

    public void setNamaPropinsi(String namaPropinsi) {
        NamaPropinsi = namaPropinsi;
    }

    public String getIdPropinsi() {
        return idPropinsi;
    }

    public String getJumlahRadio() {
        return JumlahRadio;
    }

    public String getNamaPropinsi() {
        return NamaPropinsi;
    }
}
