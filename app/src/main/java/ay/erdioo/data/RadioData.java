package ay.erdioo.data;

import org.json.JSONObject;

/**
 * Created by ibnumaksum on 4/16/15.
 */
public class RadioData{
    public String id="",nama="",propinsi="",genre="",twitter="",facebook="",
            urlStreaming="",format="",website="",rss="",telepon="",sms="",
            songId="", adUnit;
    public int hit = 0;

    public RadioData(){

    }
    public RadioData(String id,String nama,String propinsi,String genre,String twitter){
        setGenre(genre);
        setId(id);
        setNama(nama);
        setPropinsi(propinsi);
        setTwitter(twitter);
    }

    public RadioData(String id,String nama,String propinsi,String genre,String twitter,String adUnit){
        setGenre(genre);
        setId(id);
        setNama(nama);
        setPropinsi(propinsi);
        setTwitter(twitter);
        setAdUnit(adUnit);
    }

    public void setAdUnit(String adUnit) {
        this.adUnit = adUnit;
    }

    public String getAdUnit() {
        return adUnit;
    }

    public String toJson(){
        try {
            JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("nama", nama);
            json.put("propinsi", propinsi);
            json.put("genre", genre);
            json.put("twitter", twitter);
            json.put("urlStreaming", urlStreaming);
            json.put("format", format);
            json.put("website", website);
            json.put("rss", rss);
            json.put("telepon", telepon);
            json.put("sms", sms);
            json.put("adUnit",adUnit);
            return json.toString();
        }catch (Exception e){
            return "{}";
        }
    }

    public boolean setJson(String string){
        try {
            JSONObject json = new JSONObject(string);
            setId(json.getString("id"));
            setNama(json.getString("nama"));
            setPropinsi(json.getString("propinsi"));
            setGenre(json.getString("genre"));
            setTwitter(json.getString("twitter"));
            setUrlStreaming(json.getString("urlStreaming"));
            setFormat(json.getString("format"));
            setWebsite(json.getString("website"));
            setRss(json.getString("rss"));
            setTelepon(json.getString("telepon"));
            setSms(json.getString("sms"));
            setAdUnit(json.getString("adUnit"));
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public String toString() {
        return getNama()+" "+getGenre();
    }

    public String getGenre() {
        return genre;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getFormat() {
        return format;
    }

    public String getRss() {
        return rss;
    }

    public String getSms() {
        return sms;
    }

    public String getTelepon() {
        return telepon;
    }

    public String getUrlStreaming() {
        return urlStreaming;
    }

    public String getWebsite() {
        return website;
    }

    public String getSongId() {
        return songId;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setRss(String rss) {
        this.rss = rss;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public void setUrlStreaming(String urlStreaming) {
        this.urlStreaming = urlStreaming;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }
}
