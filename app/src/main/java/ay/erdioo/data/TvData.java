package ay.erdioo.data;

import org.json.JSONObject;

/**
 * Created by ibnumaksum on 4/16/15.
 */
public class TvData {
    private String id="",nama="",propinsi="",genre="",twitter="",
            urlStreaming="",website="";

    public TvData(){

    }
    public TvData(String id, String nama, String propinsi, String genre, String twitter){
        setGenre(genre);
        setId(id);
        setNama(nama);
        setPropinsi(propinsi);
        setTwitter(twitter);
    }

    public String toJson(){
        try {
            JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("nama", nama);
            json.put("propinsi", propinsi);
            json.put("genre", genre);
            json.put("twitter", twitter);
            json.put("urlStreaming", urlStreaming);
            json.put("website", website);
            return json.toString();
        }catch (Exception e){
            return "{}";
        }
    }

    public boolean setJson(String string){
        try {
            JSONObject json = new JSONObject(string);
            setId(json.getString("id"));
            setNama(json.getString("nama"));
            setPropinsi(json.getString("propinsi"));
            setGenre(json.getString("genre"));
            setTwitter(json.getString("twitter"));
            setUrlStreaming(json.getString("urlStreaming"));
            setWebsite(json.getString("website"));
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public String toString() {
        return getNama()+" "+getGenre();
    }

    public String getGenre() {
        return genre;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getUrlStreaming() {
        return urlStreaming;
    }

    public String getWebsite() {
        return website;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public void setUrlStreaming(String urlStreaming) {
        this.urlStreaming = urlStreaming;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
