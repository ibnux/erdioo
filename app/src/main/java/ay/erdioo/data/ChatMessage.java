/*
 * Dibuat pada 27/4/2016
 * Creator: Ibnu Maksum @ibnux
 * Copyright (c) 2016. PT. Asia Sarana Telekomunika
 *
 */

package ay.erdioo.data;

/**
 * Created by asatel on 27/04/16.
 */
public class ChatMessage {
    private long tgl;
    private String pesan, namaUser, warna;

    public ChatMessage(String pesan, String namaUser, long tgl){
        setPesan(pesan);
        setNamaUser(namaUser);
        setWarna(namaUser);
        setTgl(tgl);
    }

    public String getWarna() {
        if(this.warna==null || warna.length()<6 && warna != null) {
            int hash = 0;
            for (int i = 0; i < getNamaUser().length(); i++) {
                hash = getNamaUser().charAt(i) + ((hash << 5) - hash);
            }
            String c = Integer.toString(hash & 0x00FFFFFF,16);

            this.warna = "#"+ "00000".substring(0, 6 - c.length()) + c;
            //Utils.log("warna: "+warna);
        }
        return warna;
    }

    public void setWarna(String warna) {
        if(this.warna==null || warna.length()<6 && warna != null) {
            int hash = 0;
            for (int i = 0; i < warna.length(); i++) {
                hash = warna.charAt(i) + ((hash << 5) - hash);
            }
            String c = Integer.toString(hash & 0x00FFFFFF,16);

            this.warna = "#"+ "00000".substring(0, 6 - c.length()) + c;
            //Utils.log("warna: "+warna);
        }
    }

    public long getTgl() {
        return tgl;
    }

    public void setTgl(long tgl) {
        this.tgl = tgl;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
        this.setWarna(namaUser);
    }

    @SuppressWarnings("unused")
    private ChatMessage(){}

}
