package ay.erdioo.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;

import ay.erdioo.R;
import ay.erdioo.data.RadioData;
import im.delight.android.webview.AdvancedWebView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment implements AdvancedWebView.Listener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_RADIO_DATA = "RADIO_DATA";
    RadioData radioData = new RadioData();
    TextView txtUrl;

    private AdvancedWebView mWebView;

    public ChatFragment() {
        // Required empty public constructor
    }

    public static ChatFragment newInstance(int sectionNumber, String radioData) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_RADIO_DATA,radioData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            radioData.setJson(getArguments().getString(ARG_RADIO_DATA));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chat, container, false);
        txtUrl = v.findViewById(R.id.txtUrl);
        mWebView = v.findViewById(R.id.webview);
        mWebView.setListener(getActivity(), this);
        mWebView.setMixedContentAllowed(false);
        mWebView.loadUrl("http://tlk.io/"+radioData.getTwitter().replace("@","").toLowerCase());
        FirebaseAnalytics.getInstance(getContext()).logEvent("chat_"+radioData.getTwitter(),null);
        return v;
    }



    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        if(mWebView!=null)
            mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        if(mWebView!=null)
            mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if(mWebView!=null)
            mWebView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(mWebView!=null)
            mWebView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        txtUrl.setText(url);
    }

    @Override
    public void onPageFinished(String url) {
        txtUrl.setText(url);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        txtUrl.setText(failingUrl);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        try{
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }catch (Exception e){
            //
        }
    }

    @Override
    public void onExternalPageRequest(String url) {
        try{
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }catch (Exception e){
            //
        }
    }
}
