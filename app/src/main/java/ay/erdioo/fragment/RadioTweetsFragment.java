package ay.erdioo.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import ay.erdioo.R;
import ay.erdioo.activity.RadioActivity;
import ay.erdioo.data.RadioData;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RadioTweetsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RadioTweetsFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_RADIO_DATA = "RADIO_DATA";
    RadioData radioData = new RadioData();
    RadioActivity ra;
    private SwipeRefreshLayout swipe;
    WebView webView;

    public static RadioTweetsFragment newInstance(int sectionNumber, String radioData) {
        RadioTweetsFragment fragment = new RadioTweetsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_RADIO_DATA,radioData);
        fragment.setArguments(args);
        return fragment;
    }

    public RadioTweetsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ra = (RadioActivity)activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_radio_interaksi, container, false);
        webView = (WebView) v.findViewById(R.id.webViewTwitter);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        webView.setScrollContainer(false);
        webView.setBackgroundColor(0x00000000);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheMaxSize(1024 * 1024 * 8);
        webSettings.setAppCachePath(getActivity().getCacheDir().getAbsolutePath());
        webSettings.setAppCacheEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Keamanan Website tidak terjamin, Sertifikat SSL website yg dituju berbeda ");
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        webView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        radioData.setJson(getArguments().getString(ARG_RADIO_DATA));
        swipe = (SwipeRefreshLayout) v.findViewById(R.id.listViewSwipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
        if(radioData.getTwitter().length()>3)
            getData();
        return v;
    }

    public void getData(){
        if(swipe!=null)
            swipe.setRefreshing(false);
        webView.loadUrl("https://twitter.erdioo.net/timeline/?name="+radioData.getTwitter());
        FirebaseAnalytics.getInstance(getContext()).logEvent("radio_tweet_"+radioData.getTwitter(),null);
    }


}
