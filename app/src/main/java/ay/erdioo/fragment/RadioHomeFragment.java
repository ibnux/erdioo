package ay.erdioo.fragment;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import java.net.URLEncoder;

import ay.erdioo.R;
import ay.erdioo.activity.RadioActivity;
import ay.erdioo.data.RadioData;
import ay.erdioo.utils.GetDataDialog;
import ay.erdioo.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RadioHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RadioHomeFragment extends Fragment{
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_RADIO_DATA = "RADIO_DATA";
    CardView fb, tw, linkedin, mention;
    Button favorit,shortcut;
    RadioData radioData = new RadioData();
    String link="";
    RadioActivity ra;
    TextView website;
    TextView txtBerita;
    String urlBerita;


    public static RadioHomeFragment newInstance(int sectionNumber, String radioData) {
        RadioHomeFragment fragment = new RadioHomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_RADIO_DATA,radioData);
        fragment.setArguments(args);
        return fragment;
    }

    public RadioHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_radio_home, container, false);

        radioData.setJson(getArguments().getString(ARG_RADIO_DATA));
        link = "https://erdioo.net/radio/"+radioData.getId()+"/player.html";

        txtBerita = v.findViewById(R.id.txtBerita);
        fb = v.findViewById(R.id.btnFB);
        tw = v.findViewById(R.id.btnTW);
        linkedin = v.findViewById(R.id.btnLinkedin);
        mention = v.findViewById(R.id.btnMention);
        favorit =  v.findViewById(R.id.button_favorit);
        shortcut =  v.findViewById(R.id.button_addtohome);
        fb.setOnClickListener(sociallistener);
        tw.setOnClickListener(sociallistener);
        linkedin.setOnClickListener(sociallistener);
        mention.setOnClickListener(sociallistener);
        favorit.setOnClickListener(sociallistener);
        shortcut.setOnClickListener(sociallistener);

        if(radioData.getTwitter().length()<3){
            mention.setVisibility(View.GONE);
        }



        website = (TextView) v.findViewById(R.id.textViewWebsite);
        if(radioData!=null && radioData.getWebsite()!=null && radioData.getWebsite().length()>4){
            website.setText(radioData.getWebsite());
            website.setVisibility(View.VISIBLE);
        }

        txtBerita.setText("");
        txtBerita.setOnClickListener(sociallistener);

        checkBerita();

        return v;
    }

    private void checkBerita(){
        AndroidNetworking.get(Utils.getNewsRss())
                .setPriority(Priority.LOW)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String result) {
                        try{
                            String hasil = result.substring(result.indexOf("<item>")+6);
                            txtBerita.setText(
                                    (hasil.split("<description>")[1]).split("</description>")[0]
                            );
                            urlBerita = (hasil.split("<link>")[1]).split("</link>")[0];
                        }catch (Exception ee){
                            //nothing
                            ee.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

    View.OnClickListener sociallistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == tw) {
                String tweet = "";
                if(radioData.getTwitter().length()>3)
                    tweet = "#NowPlaying " + radioData.getNama() + " (@" + radioData.getTwitter() + ") Via #Erdioo for Android";
                else
                    tweet = "#NowPlaying " + radioData.getNama() + " Via #Erdioo for Android";

                FirebaseAnalytics.getInstance(getContext()).logEvent("share_np_"+radioData.getId(),null);
                Utils.postTweet(tweet,getContext());
                return;
            } else if (v == fb) {
                Utils.bukaBrowser(Uri.parse("https://www.facebook.com/sharer.php").
                        buildUpon().appendQueryParameter("u", link).build().toString(),getActivity());
                FirebaseAnalytics.getInstance(getContext()).logEvent("share_fb_"+radioData.getId(),null);
            } else if (v == txtBerita) {
                if(urlBerita!=null && urlBerita.startsWith("http")) {
                    try {
                        Utils.bukaBrowser("https://erdioo.net/news/?u="+URLEncoder.encode(urlBerita, "UTF-8"), getActivity());
                    }catch (Exception e){
                        e.printStackTrace();
                        Utils.bukaBrowser(urlBerita, getActivity());
                    }
                    checkBerita();
                    FirebaseAnalytics.getInstance(getContext()).logEvent("buka_berita_"+radioData.getId(),null);
                }
            } else if (v == linkedin) {
                Utils.bukaBrowser(Uri.parse("https://www.linkedin.com/shareArticle").
                        buildUpon().appendQueryParameter("url", link).
                        appendQueryParameter("mini", "true").build().toString(),getActivity());
                FirebaseAnalytics.getInstance(getContext()).logEvent("share_in_"+radioData.getId(),null);
            }else if (v == mention) {
                Utils.postTweet("#Erdioo @" + radioData.getTwitter() + " ",getContext());
                FirebaseAnalytics.getInstance(getContext()).logEvent("share_mention_"+radioData.getId(),null);
                /*intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://twitter.com/share").
                                buildUpon().
                                appendQueryParameter("text", "#Erdioo @" + radioData.getTwitter() + " ").build());*/
            }else if (v == favorit) {
                FirebaseAnalytics.getInstance(getContext()).logEvent("favorit_"+radioData.getId(),null);
                String idTwitter = getActivity().getSharedPreferences(Utils.PREFS_NAME, 0).getString("TwitterID", "");
                if(idTwitter.length()>2) {
                    final GetDataDialog getdialog = new GetDataDialog(getActivity());
                    getdialog.setTitle("Menambahkan ke favorit...");
                    getdialog.geturl(Utils.REST_API + "user/addFav/" +idTwitter +"/"+ radioData.getId());
                    getdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            String data = getdialog.getHasil();
                            if (data != null && data.equals("BERHASIL")) {
                                Utils.showToast("Sukses menambahkan ke favorit", getActivity());
                                SharedPreferences settings = getActivity().getSharedPreferences(Utils.PREFS_NAME, 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("favorit", "");
                                editor.putLong("favoritDate", 0);
                                editor.apply();
                                FirebaseMessaging.getInstance().subscribeToTopic("Radio"+radioData.getId());
                            } else {
                                Utils.showToast("Gagal menambahkan ke favorit", getActivity());
                            }
                        }
                    });
                }else{
                    Utils.showToast("Silahkan Login dulu di bagian Radio Favorit", getActivity());
                }
                return;
            }else if (v == shortcut) {
                FirebaseAnalytics.getInstance(getContext()).logEvent("shortcut_"+radioData.getId(),null);
                Intent shortcutIntent = new Intent(getActivity(), RadioActivity.class);
                shortcutIntent.setAction("android.intent.action.CREATE_SHORTCUT");
                shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                shortcutIntent.putExtra("dataRadio", radioData.toJson());

                BitmapDrawable drawable = (BitmapDrawable) ra.logo.getDrawable();
                Bitmap bitmap = drawable.getBitmap();

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    ShortcutManager shortcutManager = getActivity().getSystemService(ShortcutManager.class);
                    assert shortcutManager != null;
                    if (shortcutManager.isRequestPinShortcutSupported()) {
                        ShortcutInfo pinShortcutInfo =
                                new ShortcutInfo.Builder(getContext(), "radio-shortcut-" + radioData.getId())
                                        .setIntent(shortcutIntent)
                                        .setIcon(Icon.createWithAdaptiveBitmap(bitmap))
                                        .setShortLabel(radioData.getNama())
                                        .build();
                        shortcutManager.requestPinShortcut(pinShortcutInfo, null);
                    }else{
                        Utils.showToast("Tidak dapat menambahkan shortcut",getActivity());
                    }
                }else {


                    Intent addIntent = new Intent();
                    addIntent.putExtra("duplicate", false);
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
                    addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, radioData.getNama());

                    if (bitmap != null)
                        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmap);
                    else
                        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource
                                .fromContext(getActivity(), R.drawable.ic_launcher));
                    addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                    getActivity().sendBroadcast(addIntent);
                }
                return;
            }
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ra = (RadioActivity)activity;
    }

}
