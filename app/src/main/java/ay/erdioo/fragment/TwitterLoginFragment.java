package ay.erdioo.fragment;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.fragment.app.Fragment;

import ay.erdioo.MainActivity;
import ay.erdioo.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TwitterLoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TwitterLoginFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    public static TwitterLoginFragment newInstance(int sectionNumber) {
        TwitterLoginFragment fragment = new TwitterLoginFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public TwitterLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_twitter_login, container, false);
//        loginButton = (TwitterLoginButton)
//                view.findViewById(R.id.login_button);
//        loginButton.setCallback(new Callback<TwitterSession>() {
//            @Override
//            public void success(final Result<TwitterSession> result) {
//                Utils.log("Sukses Login");
//                // Do something with result, which provides a
//                // TwitterSession for making API calls
//                getActivity().getSharedPreferences(Utils.PREFS_NAME, 0).edit()
//                        .putString("TwitterID",result.data.getUserId()+"")
//                        .putString("TwitterName",result.data.getUserName())
//                        .putString("AccessToken",result.data.getAuthToken().token)
//                        .putString("AccessTokenSecret",result.data.getAuthToken().secret)
//                        .apply();
//                //new Handler().post(new Runnable() {
//                  //  @Override
//                    //public void run() {
//                        final GetDataDialog getdialog = new GetDataDialog(getActivity());
//                        getdialog.setTitle("Verifikasi login...");
//                        getdialog.geturl("https://login.erdioo.net/update.php?t=" + result.data.getAuthToken().token +
//                                "&s=" + result.data.getAuthToken().secret +
//                                "&ck=" + RadioApp.TWITTER_KEY +
//                                "&cs=" + RadioApp.TWITTER_SECRET +
//                                "&id=" + result.data.getUserId() +
//                                "&u=" + result.data.getUserName());
//                        getdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                            @Override
//                            public void onDismiss(DialogInterface dialog) {
//                                Utils.log(getdialog.getHasil());
//                                ((MainActivity) getActivity()).sudahlogin();
//                            }
//                        });
//                  //  }
//                //});
//
//
//
//            }
//
//            @Override
//            public void failure(TwitterException e) {
//                // Do something on failure
//                Utils.showToast("Gagal masuk Twitter",getActivity());
//                Utils.log("GAGAL "+e.getMessage());
//            }
//        });

        return view;
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    // ...other AnimationListener methods go here...
                });
            }
        }

        return animation;
    }

}
