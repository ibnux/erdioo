package ay.erdioo.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import ay.erdioo.MainActivity;
import ay.erdioo.R;
import ay.erdioo.activity.CariActivity;
import ay.erdioo.data.RadioData;
import ay.erdioo.data.TvData;
import ay.erdioo.utils.HttpGET;
import ay.erdioo.utils.HttpGETcallback;
import ay.erdioo.utils.RoundedTransformation;
import ay.erdioo.utils.Utils;

public class HomeFragment extends Fragment implements HttpGETcallback{
    private static final String ARG_SECTION_NUMBER = "section_number";
    String dataDown[];
    int pos = 0, jml = 0;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    TwoWayView[] listview;
    EditText editTextCari;
    View v;
    Button button_semua;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(int sectionNumber) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utils.log("Mulai create");
        v = inflater.inflate(R.layout.fragment_home, container, false);
        button_semua = v.findViewById(R.id.button_semua);
        editTextCari = (EditText)v.findViewById(R.id.editTextCari);
        editTextCari.setFocusableInTouchMode(true);
        editTextCari.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(editTextCari.getText().toString().length()>1) {
                        Intent i = new Intent(getActivity(), CariActivity.class);
                        i.putExtra("cari", editTextCari.getText().toString());
                        startActivity(i);
                    }
                    return true;
                }
                return false;
            }
        });

        if(dataDown==null) {
            dataDown = new String[]{
                    "mostviewed~" + Utils.REST_API + "radio/mostViewed",
                    "mostfavorited~" + Utils.REST_API + "radio/mostFav",
                    "todayradio~" + Utils.REST_API + "radio/random",
//                    "musicChart~https://www.langitmusik.co.id/rest/chart/daily?limit=10&page=1"
            };
            jml = dataDown.length;
            settings = getActivity().getSharedPreferences(Utils.PREFS_NAME_DATA, 0);
        }

        if(listview==null) {
            listview = new TwoWayView[dataDown.length];
            listview[0] = (TwoWayView) v.findViewById(R.id.listviewMostListened);
            listview[1] = (TwoWayView) v.findViewById(R.id.listviewMostFavorited);
            listview[2] = (TwoWayView) v.findViewById(R.id.listviewToday);
            //listview[3] = (TwoWayView) v.findViewById(R.id.listviewMusicChart);
        }

        for(int n=0;n<jml;n++){
            final int pos=n;
            listview[n].setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object obj = listview[pos].getAdapter().getItem(position);;
                    if(obj instanceof TvData){
                        TvData tv = (TvData) obj;
                    }else {
                        RadioData radio = (RadioData) obj;
                        if (radio.getId().length() > 0)
                            ((MainActivity)getActivity()).bukaRadio(radio);
                        else if (radio.getSongId().length() > 0) {
                            //https://web.melon.co.id/id/play_song/114840674
//                            Intent i = new Intent(getContext(), KeTkpActivity.class);
//                            i.putExtra("url","https://www.langitmusik.co.id/new/song/"+radio.getSongId());

                            Intent i = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://m.youtube.com/results").buildUpon()
                                            .appendQueryParameter("search_query", radio.getPropinsi() + " - " + radio.getNama()).build());

                            startActivity(i);
                        }
                    }
                }
            });
        }
        ImageView cover = (ImageView) v.findViewById(R.id.imageViewCover);
        hitungTinggi( cover);
        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawer();
            }
        });
        button_semua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).onNavigationDrawerItemSelected(3);
            }
        });
        downloadData();
        Picasso.with(getContext())
                .load("https://twitter.erdioo.net/cover/?name=erdioo")
                .fit()
                .placeholder(R.drawable.covererdioo)
                .error(R.drawable.covererdioo)
                .into(cover);

        tampilkanIklan();
        return v;
    }

    void hitungTinggi(ImageView cover){
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int imageHeight = 500;
        int imageWidth = 1500;

        int widthSize = displayMetrics.widthPixels;
        int heightSize = displayMetrics.heightPixels;

        float imageRatio = 0.0F;
        if (imageHeight > 0) {
            imageRatio = imageWidth / imageHeight;
        }
        float sizeRatio = 0.0F;
        if (heightSize > 0) {
            sizeRatio = widthSize / heightSize;
        }

        int width;
        int height;
        if (imageRatio >= sizeRatio) {
            // set width to maximum allowed
            width = widthSize;
            // scale height
            height = width * imageHeight / imageWidth;
        } else {
            // set height to maximum allowed
            height = heightSize;
            // scale width
            width = height * imageWidth / imageHeight;
        }
        cover.getLayoutParams().height = height;
        cover.getLayoutParams().width = width;
        cover.requestLayout();
    }

    private AdView mAdView;

    public void tampilkanIklan(){
        try {
            mAdView = Utils.getIklanGoogle(getContext());
            //mAdView.getLayoutParams()
            //Iklan Google
            ((LinearLayout) v.findViewById(R.id.adview)).addView(mAdView);
        }catch (Exception e){
            e.printStackTrace();
            Utils.log("tampilkanIklan error "+e.getMessage());
        }
    }

    void downloadData(){
        Utils.log("Download data ");
        if (pos < jml) {
            String[] tmp = dataDown[pos].split("~");
            String json = settings.getString(tmp[0], "[]");
            long now = (System.currentTimeMillis() / 1000) - (settings.getLong(tmp[0]+"Tgl", 0) / 1000);
            long lama = 6 * 60 * 60;
            Utils.log(now + " > " + lama);
            if (json.length() > 5) {
                Utils.log("sisa "+json.length());
                httpResultSuccess(json);
            }

            if (now > lama || json.length() < 5) {
                Utils.log("online "+json.length());
                HttpGET.doGetRequest(tmp[1], this,getContext());
                settings.edit()
                        .putString(tmp[0], "")
                        .putLong(tmp[0] + "Tgl", 0)
                        .apply();
            }
        }
    }

    @Override
    public void httpResultError(String result, int statusCode) {
        Utils.showToast("Gagal mendapatkan data dari server",getActivity());
        Utils.log("httpResultError: "+ result);
        pos++;
        downloadData();
    }

    @Override
    public void httpResultSuccess(final String result) {
        Utils.log(result);
        String[] tmp = null;
        try {
            tmp = dataDown[pos].split("~");
            if(tmp[0].equals("musicChart")){
                JSONObject jobject = new JSONObject(result);
                JSONArray jArray = jobject.getJSONArray("dataList");
                int jm = jArray.length();
                final RadioData[] radioDatas = new RadioData[jm];
                for (int n = 0; n < jm; n++) {
                    JSONObject obj = jArray.getJSONObject(n);
                    radioDatas[n] = new RadioData("",
                            obj.getString("songName"),
                            obj.getString("artistName"),
                            "",
                            "");
                    radioDatas[n].setSongId(obj.getString("songId"));
                }
                //https://web.melon.co.id/id/play_song/114840674
                listview[pos].setAdapter(new ArrayAdapter<RadioData>(
                        getActivity().getBaseContext(),
                        R.layout.radio_item,
                        R.id.radio_nama,
                        radioDatas
                ) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        ViewHolder holder;
                        View view = convertView;
                        if (view == null) {
                            //View view = super.getView(position, convertView, parent);
                            LayoutInflater vi = (LayoutInflater) getContext()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            view = vi.inflate(R.layout.radio_item, null);
                            holder = new ViewHolder();
                            holder.nama = (TextView) view.findViewById(R.id.radio_nama);
                            holder.propinsi = (TextView) view.findViewById(R.id.radio_propinsi);
                            holder.icon = (ImageView) view.findViewById(R.id.imageRadio);
                            view.setTag(holder);
                        } else
                            holder = (ViewHolder) view.getTag();
                        RadioData radioData = getItem(position);
                        holder.nama.setText(radioData.getNama().replaceAll("(?i)radio", ""));
                        holder.propinsi.setText(radioData.getPropinsi());
                        if (radioData.getSongId().length() > 0)
                            Picasso.with(getActivity())
                                    .load("https://www.langitmusik.co.id/imageSong.do?size=S&songId=" + radioData.getSongId())
                                    .fit()
                                    .placeholder(R.drawable.ic_launcher)
                                    .error(R.drawable.ic_launcher)
                                    .transform(new RoundedTransformation(10, 0))
                                    .into(holder.icon);
                        return view;
                    }
                });
            }else {
                JSONArray jArray = new JSONArray(result);
                int jm = jArray.length();
                final RadioData[] radioDatas = new RadioData[jm];
                for (int n = 0; n < jm; n++) {
                    JSONObject obj = jArray.getJSONObject(n);
                    radioDatas[n] = new RadioData(obj.getString("IdRadio"),
                            obj.getString("NamaRadio"),
                            obj.getString("Lokasi"),
                            obj.getString("Genre"),
                            obj.getString("Twitter"));
                }
                listview[pos].setAdapter(new ArrayAdapter<RadioData>(
                        getActivity().getBaseContext(),
                        R.layout.radio_item,
                        R.id.radio_nama,
                        radioDatas
                ) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        ViewHolder holder;
                        View view = convertView;
                        if(view==null) {
                            //View view = super.getView(position, convertView, parent);
                            LayoutInflater vi = (LayoutInflater) getContext()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            view = vi.inflate(R.layout.radio_item, null);
                            holder = new ViewHolder();
                            holder.nama = (TextView) view.findViewById(R.id.radio_nama);
                            holder.propinsi = (TextView) view.findViewById(R.id.radio_propinsi);
                            holder.icon = (ImageView) view.findViewById(R.id.imageRadio);
                            view.setTag(holder);
                        }else
                            holder = (ViewHolder) view.getTag();

                        RadioData radioData = getItem(position);
                        holder.nama.setText(radioData.getNama().replaceAll("(?i)radio", ""));
                        if(radioData.getPropinsi().length()>1) {
                            holder.propinsi.setVisibility(View.VISIBLE);
                            holder.propinsi.setText(radioData.getPropinsi());
                        }else
                            holder.propinsi.setVisibility(View.GONE);
                        if (radioData.getTwitter().length() > 4)
                            Picasso.with(getActivity())
                                    .load("https://twitter.erdioo.net/avatar/" + radioData.getTwitter() + "/bigger.jpg")
                                    .fit()
                                    .placeholder(R.drawable.ic_launcher)
                                    .error(R.drawable.ic_launcher)
                                    .transform(new RoundedTransformation(10, 0))
                                    .into(holder.icon);
                        //view.setTag(radioData);
                        return view;
                    }
                });
            }

            settings.edit()
                    .putString(tmp[0], result)
                    .putLong(tmp[0] + "Tgl", System.currentTimeMillis())
                    .apply();

        }catch (Exception e){
            Utils.log("Error: "+e.getMessage());
            if(tmp!=null) {
                settings.edit()
                        .remove(tmp[0])
                        .remove(tmp[0] + "Tgl")
                        .apply();
            }
        }

        pos++;
        downloadData();

    }

    private static class ViewHolder{
        public TextView nama,propinsi;
        public ImageView icon;
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    // ...other AnimationListener methods go here...
                });
            }
        }

        return animation;
    }
}
