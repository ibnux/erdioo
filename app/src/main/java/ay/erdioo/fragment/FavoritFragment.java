package ay.erdioo.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import ay.erdioo.MainActivity;
import ay.erdioo.R;
import ay.erdioo.data.RadioData;
import ay.erdioo.utils.GetDataDialog;
import ay.erdioo.utils.HttpGET;
import ay.erdioo.utils.HttpGETcallback;
import ay.erdioo.utils.RoundedTransformation;
import ay.erdioo.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoritFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritFragment extends Fragment implements HttpGETcallback {
    private ListView listView;
    private SwipeRefreshLayout swipe;
    ArrayAdapter adapter;
    EditText txtcari;
    SharedPreferences settings;

    private static final String ARG_SECTION_NUMBER = "section_number";
    public static FavoritFragment newInstance(int sectionNumber) {
        FavoritFragment fragment = new FavoritFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FavoritFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_favorit, container, false);
        settings = getActivity().getSharedPreferences(Utils.PREFS_NAME, 0);

        swipe = (SwipeRefreshLayout) v.findViewById(R.id.listViewSwipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HttpGET.doGetRequest(Utils.REST_API + "user/fav/" + settings.getString("TwitterID",""), FavoritFragment.this, getContext());
                swipe.setRefreshing(true);
            }
        });
        listView = (ListView) v.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RadioData radio = (RadioData) listView.getAdapter().getItem(position);
                if (radio != null) {
                    ((MainActivity)getActivity()).bukaRadio(radio);
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final RadioData radio = (RadioData) listView.getAdapter().getItem(position);
                if (radio != null) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage("Hapus dari daftar favorit?");
                    alert.setTitle(radio.getNama());
                    alert.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            final GetDataDialog getdialog = new GetDataDialog(getActivity());
                            getdialog.setTitle("Menghapus dari favorit...");
                            getdialog.geturl(Utils.REST_API + "user/delFav/"+ settings.getString("TwitterID","") + "/" + radio.getId() );
                            getdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    String data = getdialog.getHasil();
                                    if (data != null && data.length()>1) {
                                        Utils.showToast("Sukses menghapus dari favorit", getActivity());
                                        FirebaseMessaging.getInstance().unsubscribeFromTopic("Radio"+radio.getId());
                                        HttpGET.doGetRequest(Utils.REST_API + "user/fav/" + settings.getString("TwitterID",""), FavoritFragment.this, getContext());
                                        swipe.setRefreshing(true);
                                    } else {
                                        Utils.showToast("Gagal menambahkan ke favorit", getActivity());
                                    }
                                }
                            });
                        }
                    });

                    alert.setNegativeButton("Batal",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            });
                    alert.show();
                }
                return true;
            }
        });

        txtcari = (EditText) v.findViewById(R.id.editTextCari);
        txtcari.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                FavoritFragment.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {}
        });

        swipe.setRefreshing(true);

        String json = settings.getString("favorit", "[]");
        long now = (System.currentTimeMillis() / 1000) - (settings.getLong("favoritDate", 0) / 1000);
        long lama = 2* 24 * 60 * 60;
        Utils.log(now + " > " + lama);
        if (json.length() > 5) {
            httpResultSuccess(json);
        }

        if (now > lama) {
            HttpGET.doGetRequest("user/fav/" + settings.getString("TwitterID",""), this,getContext());
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("favorit", "");
            editor.putLong("favoritDate", 0);
            editor.commit();
        }

        FirebaseAnalytics.getInstance(getContext()).logEvent("favorit_page",null);

        //Iklan Google
        mAdView = Utils.getIklanGoogle(getContext());
        ((LinearLayout)v.findViewById(R.id.erdiooad)).addView(mAdView);
        return v;
    }

    AdView mAdView;

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void httpResultSuccess(String result) {
        try{
            JSONArray jArray = new JSONArray(result);
            int jm = jArray.length();
            if(jm>25) txtcari.setVisibility(View.VISIBLE);
            RadioData[] radioDatas = new RadioData[jm];
            SharedPreferences topik = getActivity().getSharedPreferences("TOPIC",0);
            for (int n = 0; n < jm; n++) {
                JSONObject obj = jArray.getJSONObject(n);
                radioDatas[n] = new RadioData(obj.getString("IdRadio"),
                        obj.getString("NamaRadio"),
                        obj.getString("Lokasi"),
                        obj.getString("Genre"),
                        obj.getString("Twitter"));
                if(!topik.getBoolean(obj.getString("IdRadio"),false)){
                    FirebaseMessaging.getInstance().subscribeToTopic("Radio"+obj.getString("IdRadio"));
                    topik.edit().putBoolean(obj.getString("IdRadio"),true).commit();
                }
            }

            adapter = new ArrayAdapter<RadioData>(
                    getActivity(),
                    R.layout.item_menu_duatext_satugambar,
                    R.id.textBesar,
                    radioDatas
            ) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = convertView;
                    ViewHolder holder;
                    if(view==null) {
                        LayoutInflater vi = (LayoutInflater) getContext()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = vi.inflate(R.layout.item_menu_duatext_satugambar, null);
                        //View view = super.getView(position, convertView, parent);
                        holder = new ViewHolder();
                        holder.nama = (TextView) view.findViewById(R.id.textBesar);
                        holder. genre = (TextView) view.findViewById(R.id.textKecil);
                        holder. icon = (ImageView) view.findViewById(R.id.icon);
                        view.setTag(holder);
                    }else
                        holder = (ViewHolder)view.getTag();

                    RadioData radio = getItem(position);
                    holder.nama.setText(radio.getNama());
                    holder.genre.setText(radio.getGenre());
                    if (radio.getTwitter().length() > 4)
                        Picasso.with(getActivity())
                                .load("https://twitter.erdioo.net/avatar/" + radio.getTwitter() + "/bigger.jpg")
                                .fit()
                                .placeholder(R.drawable.ic_launcher)
                                .error(R.drawable.ic_launcher)
                                .transform(new RoundedTransformation(4, 0))
                                .into(holder.icon);
                    return view;
                }
            };

            listView.setAdapter(adapter);
            SharedPreferences settings = getActivity().getSharedPreferences(Utils.PREFS_NAME, 0);
            if (!settings.getString("favorit", "[]").equals(result)) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("favorit", result);
                editor.putLong("favoritDate", System.currentTimeMillis());
                editor.apply();
            }
        }catch (Exception e){
            Utils.log("ERROR "+e.getMessage());
        }
        swipe.setRefreshing(false);
    }
    private static class ViewHolder{
        public TextView nama,genre;
        public ImageView icon;
    }
    @Override
    public void httpResultError(String result, int statusCode) {
        Utils.showToast("Gagal mengambil data Favorit", getActivity());
        swipe.setRefreshing(false);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    // ...other AnimationListener methods go here...
                });
            }
        }

        return animation;
    }

}
