package ay.erdioo.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdView;

import ay.erdioo.MainActivity;
import ay.erdioo.R;
import ay.erdioo.utils.Utils;
import im.delight.android.webview.AdvancedWebView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsBrowserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsBrowserFragment extends Fragment implements AdvancedWebView.Listener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    public AdvancedWebView mWebView;
    ProgressBar progressBar;

    public static NewsBrowserFragment newInstance(int sectionNumber, String url) {
        NewsBrowserFragment fragment = new NewsBrowserFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString("url",url);
        fragment.setArguments(args);
        return fragment;
    }

    public NewsBrowserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_news_browser, container, false);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        progressBar.setMax(100);
        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        mWebView = v.findViewById(R.id.webview);
        mWebView.setWebChromeClient(new WebChromeClient() {

                                       public void onProgressChanged(WebView view, int progress) {
                                           progressBar.setIndeterminate(false);
                                           progressBar.setMax(100);
                                           progressBar.setProgress(progress);
                                       }
                                   }
        );

        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                Utils.log("Resource: "+url);
            }
        });
        mWebView.setListener(getActivity(), this);
        mWebView.addHttpHeader("user-agent","Mozilla/5.0 (Linux; Android 10; SM-G970F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3396.81 Mobile Safari/537.36");
        Utils.log("loadUrl: "+Utils.REST_API+"/news");
        mWebView.loadUrl(Utils.REST_API+"/news");
        //Iklan Google
        mAdView = Utils.getIklanGoogle(getContext());
        ((LinearLayout)v.findViewById(R.id.erdiooad)).addView(mAdView);
        return v;
    }

    AdView mAdView;


    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
        mWebView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        mWebView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mWebView.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        Utils.log("onPageStarted: "+url);
        if(!url.contains("erdioo") && !url.contains("kumparan")){
            Utils.bukaBrowser(url,getActivity());
            mWebView.goBack();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
    }

    @Override
    public void onPageFinished(String url) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        Utils.bukaBrowser(url,getActivity());
    }

    @Override
    public void onExternalPageRequest(String url) {
        Utils.bukaBrowser(url,getActivity());
    }


    public boolean goBack(){
        if(mWebView.canGoBack()){
            mWebView.goBack();
            return true;
        }else
            return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_browser, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_browser) {
            Intent intent = intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(mWebView.getUrl()));
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    // ...other AnimationListener methods go here...
                });
            }
        }

        return animation;
    }

}
