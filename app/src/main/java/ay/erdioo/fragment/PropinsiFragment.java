package ay.erdioo.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONObject;

import ay.erdioo.MainActivity;
import ay.erdioo.R;
import ay.erdioo.activity.PropinsiActivity;
import ay.erdioo.data.PropinsiData;
import ay.erdioo.utils.HttpGET;
import ay.erdioo.utils.HttpGETcallback;
import ay.erdioo.utils.Utils;


public class PropinsiFragment extends Fragment implements HttpGETcallback {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ListView listView;
    private SwipeRefreshLayout swipe;

    public static PropinsiFragment newInstance(int sectionNumber) {
        PropinsiFragment fragment = new PropinsiFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PropinsiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_propinsi, container, false);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.listViewSwipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HttpGET.doGetRequest(Utils.REST_API + "propinsi", PropinsiFragment.this,getContext());
                swipe.setRefreshing(true);
            }
        });
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PropinsiData propinsi = (PropinsiData) listView.getAdapter().getItem(position);
                if(propinsi!=null){
                    Intent i = new Intent(getActivity(), PropinsiActivity.class);
                    i.putExtra("idPropinsi",propinsi.getIdPropinsi());
                    i.putExtra("namaPropinsi",propinsi.getNamaPropinsi());
                    startActivity(i);
                }
            }
        });

        FirebaseAnalytics.getInstance(getContext()).logEvent("buka_propinsi",null);

        //Iklan Google
        ((LinearLayout)view.findViewById(R.id.adview)).addView(Utils.getIklanGoogle(getContext()));


        SharedPreferences settings = getActivity().getSharedPreferences(Utils.PREFS_NAME, 0);
        String json = settings.getString("propinsi", "[]");
        long now = (System.currentTimeMillis() / 1000) - (settings.getLong("propinsiDate", 0) / 1000);
        long lama = 30* 24 * 60 * 60;
        Utils.log(now + " > " + lama);
        if (json.length() > 5) {
            httpResultSuccess(json);
        }

        if (now > lama) {
            //HttpGET.doGetRequest(Utils.APIURL+"json/?do=listBerita",this);
            HttpGET.doGetRequest(Utils.REST_API+"propinsi", this,getContext());
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("propinsi", "");
            editor.putLong("propinsiDate", 0);
            editor.commit();
        }

        return view;
    }

    @Override
    public void httpResultError(String result, int statusCode) {
        swipe.setRefreshing(false);
        Utils.showToast(result, getActivity());
    }

    @Override
    public void httpResultSuccess(String result) {
        swipe.setRefreshing(false);
        try{
            JSONArray jArray = new JSONArray(result);
            int jml = jArray.length();
            PropinsiData[] propinsiDatas = new PropinsiData[jml];
            for (int n = 0; n < jml; n++) {
                JSONObject obj = jArray.getJSONObject(n);
                propinsiDatas[n] = new PropinsiData(obj.getString("idPropinsi"),obj.getString("NamaPropinsi"),obj.getString("JumlahRadio"));
            }
            listView.setAdapter(new ArrayAdapter<PropinsiData>(
                    getActivity().getBaseContext(),
                    R.layout.item_menu_duatext_satugambar,
                    R.id.textBesar,
                    propinsiDatas
            ) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = convertView;
                    ViewHolder holder;
                    if(view==null) {
                        LayoutInflater vi = (LayoutInflater) getContext()
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = vi.inflate(R.layout.item_menu_duatext_satugambar, null);
                        //View view = super.getView(position, convertView, parent);
                        holder = new ViewHolder();
                        holder.nama = (TextView) view.findViewById(R.id.textBesar);
                        holder. genre = (TextView) view.findViewById(R.id.textKecil);
                        holder. icon = (ImageView) view.findViewById(R.id.icon);
                        view.setTag(holder);
                    }else
                        holder = (ViewHolder)view.getTag();

                    PropinsiData propinsi = getItem(position);
                    holder.nama.setText(propinsi.getNamaPropinsi());
                    holder.genre.setText(propinsi.getJumlahRadio() + " Stasiun Radio");
                    String huruf = propinsi.getNamaPropinsi().substring(0,1);
                    holder.icon.setImageResource(getResources().getIdentifier(huruf.toLowerCase(), "drawable", getActivity().getPackageName()));

                    return view;
                }
            });
            SharedPreferences settings = getActivity().getSharedPreferences(Utils.PREFS_NAME, 0);
            if (!settings.getString("propinsi", "[]").equals(result)) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("propinsi", result);
                editor.putLong("propinsiDate", System.currentTimeMillis());
                editor.commit();
            }
        }catch (Exception e){
            Utils.log(e.getMessage());
        }
    }
    private static class ViewHolder{
        public TextView nama,genre;
        public ImageView icon;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    // ...other AnimationListener methods go here...
                });
            }
        }

        return animation;
    }

}
