package ay.erdioo.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.TwitterAuthProvider;

import ay.erdioo.MainActivity;
import ay.erdioo.R;
import ay.erdioo.utils.Utils;
import im.delight.android.webview.AdvancedWebView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginTwitterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginTwitterFragment extends Fragment implements AdvancedWebView.Listener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private AdvancedWebView mWebView;
    ProgressBar progressBar;
    TextView status;



    public static LoginTwitterFragment newInstance(int sectionNumber) {
        LoginTwitterFragment fragment = new LoginTwitterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public LoginTwitterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_twitter, container, false);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        status = v.findViewById(R.id.status);
        progressBar.setIndeterminate(true);
        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        mWebView = v.findViewById(R.id.webViewLoginTwitter);
        mWebView.setWebChromeClient(new WebChromeClient() {

                                       public void onProgressChanged(WebView view, int progress) {
                                           progressBar.setIndeterminate(false);
                                           progressBar.setMax(100);
                                           progressBar.setProgress(progress);
                                       }
                                   }
        );
        mWebView.setListener(getActivity(), this);
        mWebView.loadUrl("https://login.erdioo.net/?id=" + getActivity().getSharedPreferences(Utils.PREFS_NAME, 0).getString("deviceId", ""));
        mWebView.addJavascriptInterface(new tokenJSInterface(), "AndroidFunction");

        return v;
    }

    public class tokenJSInterface {

        @JavascriptInterface
        public void sendToken(String twitter_id, String twitter_name, String token, String secret) {
            getActivity().getSharedPreferences(Utils.PREFS_NAME, 0).edit()
                    .putString("TwitterID",twitter_id)
                    .putString("TwitterName",twitter_name)
                    .putString("AccessToken",token)
                    .putString("AccessTokenSecret",secret)
                    .apply();
            FirebaseAnalytics.getInstance(getContext()).setUserId(twitter_id);
            FirebaseAnalytics.getInstance(getContext()).setUserProperty("TwitterName","twitter_name");

            AuthCredential credential = TwitterAuthProvider.getCredential(token, secret);
            FirebaseAuth.getInstance().signInWithCredential(credential)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task task) {
                            //Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((MainActivity) getActivity()).sudahlogin();
                                }
                            });

                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                //Log.w(TAG, "signInWithCredential", task.getException());
                                Utils.log("Login Gagal");
                            }else{
                                Utils.log("Login sukses");
                            }

                            // ...
                        }
                    });

        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    // ...other AnimationListener methods go here...
                });
            }
        }

        return animation;
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mWebView.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        status.setText(url);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
    }

    @Override
    public void onPageFinished(String url) {
        status.setText(url);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        status.setText(failingUrl);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        Utils.bukaBrowser(url,getActivity());
    }

    @Override
    public void onExternalPageRequest(String url) {
        Utils.bukaBrowser(url,getActivity());
    }

}
