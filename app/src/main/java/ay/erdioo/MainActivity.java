package ay.erdioo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;
import java.util.UUID;

import ay.erdioo.activity.RadioActivity;
import ay.erdioo.data.RadioData;
import ay.erdioo.fragment.FavoritFragment;
import ay.erdioo.fragment.InfoFragment;
import ay.erdioo.fragment.LoginTwitterFragment;
import ay.erdioo.fragment.NewsBrowserFragment;
import ay.erdioo.fragment.PropinsiFragment;
import ay.erdioo.fragment.RadioFragment;
import ay.erdioo.layanan.RadioService;
import ay.erdioo.utils.Utils;


public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private String[] menuKiri;
    int pos=0;
    PropinsiFragment propinsiFragment;
    FavoritFragment favoritFragment;
    NewsBrowserFragment newsBrowserFragment;
    //TwitterLoginFragment twitterLoginFragment;

    private InterstitialAd mInterstitialAd;
    private RadioData radioData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
// will use fetch data from the Remote Config service, rather than cached parameter values,
// if cached parameter values are more than cacheExpiration seconds old.
// See Best Practices in the README for more information.
        FirebaseRemoteConfig.getInstance().fetch()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            RadioApp.disableIklan = FirebaseRemoteConfig.getInstance().getBoolean("matikanklan");
                        }
                    }
                });


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Utils.isLogin = true;
                    Utils.log("Sudah masuk");
                } else {
                    Utils.isLogin = false;
                    Utils.log("Belum masuk / logout");
                }
            }
        };

        // Create the InterstitialAd and set the adUnitId.
        InterstitialAd.load(this,
                Utils.getAdUnitIDInter(),
                Utils.getAdRequest((radioData != null) ? radioData.nama + " " + radioData.genre : ""), new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        super.onAdLoaded(interstitialAd);
                        mInterstitialAd = interstitialAd;

                        mInterstitialAd.setFullScreenContentCallback(MainActivity.this.fullScreenContentCallback);
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                    }
                });


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        SharedPreferences settings = getSharedPreferences(Utils.PREFS_NAME, 0);
        if (!settings.contains("deviceId"))
            settings.edit().putString("deviceId", UUID.randomUUID().toString()).apply();

        /*Intent i = new Intent(this, RadioService.class);
        i.putExtra("namaRadio","GenFM");
        i.putExtra("idRadio","1");
        i.putExtra("genreRadio","Rock Metal");
        i.putExtra("urlRadio","https://sportku.com:8000/genfm");
        i.setAction(RadioService.ACTION_PLAY);
        startService(i);*/
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        Dexter.withContext(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        //Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.INSTALL_SHORTCUT,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.VIBRATE,
                        Manifest.permission.FOREGROUND_SERVICE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();

        if(Utils.isLogin){
            FirebaseAnalytics.getInstance(this).setUserId(getSharedPreferences(Utils.PREFS_NAME, 0).getString("TwitterID",""));
        }
    }

    FullScreenContentCallback fullScreenContentCallback = new FullScreenContentCallback(){
        @Override
        public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
            openRadio();
        }

        @Override
        public void onAdDismissedFullScreenContent() {
            openRadio();
        }
    };



    public void openDrawer(){
        mNavigationDrawerFragment.openDrawer();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        int pos = position + 1;
        this.pos = position;
        if (menuKiri == null)
            menuKiri = getResources().getStringArray(R.array.menu_kiri);
        if(position==0)
            mTitle = "Radio Internet Indonesia";
        else
            mTitle = menuKiri[position];
        restoreActionBar();
       FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fr;
        if (pos == 1) {
            fr = RadioFragment.newInstance();
            getSupportActionBar().hide();
        }else if (pos == 2) {
            if (getSharedPreferences(Utils.PREFS_NAME, 0).getString("TwitterID", "").length() > 2) {
                if(favoritFragment==null) favoritFragment = FavoritFragment.newInstance(pos);
                fr = favoritFragment;
            } else {
                fr = LoginTwitterFragment.newInstance(pos);
            }
            /*if (getSharedPreferences(Utils.PREFS_NAME, 0).getString("TwitterID", "").length() > 2) {
                if(favoritFragment==null) favoritFragment = FavoritFragment.newInstance(pos);
                fr = favoritFragment;
            } else {
                if(twitterLoginFragment==null) twitterLoginFragment = TwitterLoginFragment.newInstance(pos);
                fr = twitterLoginFragment;
                tag = "login";
            }*/
            getSupportActionBar().show();
        }else if (pos == 3) {
            if(newsBrowserFragment==null) newsBrowserFragment = NewsBrowserFragment.newInstance(pos,"");
            fr =  newsBrowserFragment;
            getSupportActionBar().hide();
        }else if (pos == 4) {
            if(propinsiFragment==null) propinsiFragment = PropinsiFragment.newInstance(pos);
            fr =  propinsiFragment;
            getSupportActionBar().show();
        }else if (pos == 5) {
            fr = InfoFragment.newInstance(pos);
            getSupportActionBar().hide();
        }else if (pos == 6) {
            FirebaseAnalytics.getInstance(this).logEvent("klik_daftar",null);
            Intent intent = intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://erdioo.net/form_registrasi/"));
            startActivity(intent);
            return;
        }else if(pos == 7){
            FirebaseAnalytics.getInstance(this).logEvent("dm_erdioo",null);
            Utils.bukaBrowser("https://twitter.com/messages/compose?recipient_id=289373425",this);
            getSupportActionBar().hide();
            return;
        } else {
            fr = PlaceholderFragment.newInstance(position + 1);
            getSupportActionBar().show();
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fr)
                .commit();
    }

    public void sudahlogin(){
        onNavigationDrawerItemSelected(1);
    }

    public void onSectionAttached(int number) {
        if (menuKiri == null)
            menuKiri = getResources().getStringArray(R.array.menu_kiri);
        if(number-1==0)
            mTitle = "Radio Internet Indonesia";
        else
            mTitle = menuKiri[number - 1];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.global, menu);
            restoreActionBar();
            return true;
        }
        menu.clear();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_daftar) {
            FirebaseAnalytics.getInstance(this).logEvent("klik_daftar",null);
            Intent intent = intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://erdioo.net/form_registrasi/"));
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Utils.log("KEYCODE_BACK");
            Utils.log("posisi" + mNavigationDrawerFragment.getPosition());

            if(mNavigationDrawerFragment.getPosition()==2){
                Utils.log("newsBrowserFragment.goBack");
                if(newsBrowserFragment.goBack()){
                    return true;
                }
            }else if(mNavigationDrawerFragment.getPosition()==6){
                Utils.log("kontakFragment.webView.canGoBack");

            }
            if (mNavigationDrawerFragment!=null && mNavigationDrawerFragment.getPosition() > 0) {
                mNavigationDrawerFragment.goHome();
                return true;
            }else{
                if (getSharedPreferences(Utils.PREFS_NAME, 0).getBoolean("sudahRate", false)) {
                    new AlertDialog.Builder(this)
                            .setIcon(R.mipmap.ic_launcher)
                            .setTitle(R.string.app_name)
                            .setMessage(R.string.dialog_tanya_tutup)
                            .setNegativeButton(R.string.dialog_matikan_aplikasi, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(getApplicationContext(), RadioService.class);
                                    i.setAction(RadioService.ACTION_STOP);
                                    startService(i);
                                    finish();
                                }

                            })
                            .setPositiveButton(R.string.dialog_tutup_aplikasi, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                } else {
                    new AlertDialog.Builder(this)
                            .setIcon(R.mipmap.ic_launcher)
                            .setTitle(R.string.app_name)
                            .setMessage(R.string.dialog_tanya_tutup)
                            .setNegativeButton(R.string.dialog_matikan_aplikasi, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(getApplicationContext(), RadioService.class);
                                    i.setAction(RadioService.ACTION_STOP);
                                    startService(i);
                                    finish();
                                }

                            })
                            .setPositiveButton(R.string.dialog_tutup_aplikasi, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setNeutralButton(R.string.dialog_rate_aplikasi, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getSharedPreferences(Utils.PREFS_NAME, 0).edit().putBoolean("sudahRate", true).commit();
                                    try {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("market://details?id=ay.erdioo"));
                                        startActivity(intent);

                                    } catch (Exception e) {
                                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=ay.erdioo"));
                                        startActivity(i);
                                    }
                                    FirebaseAnalytics.getInstance(MainActivity.this).logEvent("klik_rating",null);
                                    finish();
                                }
                            })
                            .show();
                }
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(radioData!=null)
            openRadio();
    }

    private void showInterstitial() {
        Utils.log("showInterstitial");
        openRadio();
        if (mInterstitialAd != null) {
            RadioActivity.isAd = true;
            openRadio();
            if(!RadioApp.disableIklan && !RadioApp.isClicked) {
                RadioApp.isClicked = true;
                mInterstitialAd.show(MainActivity.this);
            }else {
                RadioApp.isClicked = false;
            }
        } else {
            openRadio();
        }
    }


    public void bukaRadio(RadioData radioData){
        RadioApp.clicked++;
        this.radioData = radioData;
        if (android.os.Build.VERSION.SDK_INT <21){
            openRadio();
            return;
        }
        RadioActivity.isAd = false;
        Utils.log("bukaRadio: "+RadioApp.clicked);
        if(!radioData.getGenre().toLowerCase().contains("religi") && RadioApp.clicked%3==0)
            showInterstitial();
        else openRadio();
    }

    private void openRadio(){
        if(radioData!=null) {
            Intent intent = new Intent(this, RadioActivity.class);
            intent.putExtra("dataRadio", radioData.toJson());
            startActivity(intent);
            this.radioData = null;
        }
    }

}
